# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.1.9] - 2024-02-13

### Fixed

-   Fix Honeypot ([golemio-bi#61]https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/61)

## [0.1.8] - 2024-02-07

### Removed

-   Remove Recaptcha ([golemio-bi#61]https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/61)

## [0.1.7] - 2024-02-05

### Added

-   Tracking dashboard visits ([frontend#48](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/48))

## [0.1.6] - 2023-09-25

### Added

-   New container type added to sorted waste

### Changed

-   Only showing public accessible containers

## [0.1.5] - 2023-09-14

### Fixed

-   Sign-out on refresh ([permission-proxy#160](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/160))

## [0.1.4] - 2023-09-13

### Removed

-   Unnecessary info from JWT token ([permission-proxy#160](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/160))

## [0.1.3] - 2023-06-21

### Added

-   gtm added back for UX analytics

### Changed

-   logo chnaged to new OICT logo

## [0.1.2] - 2023-06-7

### Fixed

-   Parking map data transformation ([parkings#11](https://gitlab.com/operator-ict/golemio/projekty/oict/parkings/-/issues/11))
-   Map icon loading

## [0.1.1] - 2022-11-14

No changelog
