window.ENV = {
    API_URL: "https://api-bi.rabin.golemio.cz",
    AUTH_API_URL: "https://permissions.rabin.golemio.cz/gateway",
    PROXY_API_URL: "https://rabin.golemio.cz",
    PUBLIC_URL: "http://localhost:3000/",
    RECAPTCHA_SITE_KEY: "6LdI7TcbAAAAAKmUW6x38yvbBa5jcfl1l7G71NsK",
    MAPBOX_TOKEN: "__fill_me__",
    DEFAULT_LANG: "cs",
    GA_KEY: "",
    GTM_ID: "",
    DATA_DOMAIN: "localhost:3000",
};
