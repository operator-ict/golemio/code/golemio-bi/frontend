ARG BASE_IMAGE=node:18.17.0-alpine

FROM $BASE_IMAGE AS builder
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install 
COPY . .
RUN export PUBLIC_URL="/"; \
    export NODE_PATH=./src; \
    npm run build

FROM $BASE_IMAGE
WORKDIR /app
RUN npm install -g serve@14.1.2

COPY --from=builder /app/build .

# Create a non-root user
RUN addgroup -g 1001 -S nonroot &&\
    adduser -S nonroot -u 1001 && \
    chown -R nonroot /app
USER nonroot

EXPOSE 3000
CMD ["serve","-s", "-l", "3000"]