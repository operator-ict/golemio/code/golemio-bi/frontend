declare interface Window {
  plausible: (...args: any[]) => void;

  ENV: {
    API_URL: string;
    AUTH_API_URL: string;
    PROXY_API_URL: string;
    RECAPTCHA_SITE_KEY: string;
    MAPBOX_TOKEN: string;
    DEFAULT_LANG: "cs" | "en";
    PUBLIC_URL: string;
    GA_KEY: string;
    GTM_ID: string;
  };
}
