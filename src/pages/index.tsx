import { useEffect, Suspense } from "react";
import { Routes, Route, Navigate, useLocation } from "react-router-dom";

import { AuthRoutes } from "./Auth";
import { Dashboard } from "./Dashboard";
import PageTermsAndConditions from "./TermsAndConditions";
import { appState } from "state";
import { AuthStateContext } from "state/modules/auth";
import { useAnalytics } from "hooks/analytics";
import TagManager from "react-gtm-consent-module";

export const AppPages = () => {
    const { pathname, search } = useLocation();
    const { pageview } = useAnalytics();

    const userId = appState?.auth?.user?.id

    const tagManagerArgs = {
    dataLayer: {
    user_id: userId,
    userProject: 'golemio-bi',
    event: 'user_idSet',
    },
    dataLayerName: 'user_id'
    }

    useEffect(() => {
        TagManager.dataLayer(tagManagerArgs)
    });

    useEffect(() => {
        pageview(`${pathname}${search}`);
    }, [pathname, search, pageview]);

    return (
        <Suspense fallback={<div />}>
            <AuthStateContext.Provider value={appState}>
                <Routes>
                    <Route path="/status" element={<p>Running...</p>}></Route>
                    <Route path="/terms" element={<PageTermsAndConditions />} />
                    <Route path="/dashboard/*" element={<Dashboard />} />
                    <Route
                        path="/auth/*"
                        element={<AuthRoutes enableSignUp={false} />}
                    />
                    <Route
                        path="*"
                        element={<Navigate to="/auth/sign-in" replace />}
                    />
                </Routes>
            </AuthStateContext.Provider>
        </Suspense>
    );
};
