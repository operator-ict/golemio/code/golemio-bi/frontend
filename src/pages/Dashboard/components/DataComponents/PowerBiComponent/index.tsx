import React, { FC, useEffect, useRef, useState } from "react";
import { MetadataIO } from "codecs";
import * as io from "io-ts";
import { observer } from "mobx-react-lite";
import { AppLayout } from "components/AppLayout";
import { DashboardHeader } from "pages/Dashboard/components/DashboardHeader";
import { ContainerVertical, ContentLoader } from "components";
import { appState } from "state";
import { IReportEmbedConfiguration, models } from "powerbi-client";
import { IconButton } from "@mui/material";
import { Fullscreen } from "@mui/icons-material";
import { useTranslation } from "react-i18next";
import { makeStyles } from "tss-react/mui";

const MINUTES_BEFORE_EXPIRATION = 5;

const PowerBiDataIO = io.intersection([
  io.interface({
    report_id: io.string,
    workspace_id: io.string,
  }),
  io.partial({
    page_id: io.string,
  }),
]);

export const PowerBiMetadataIO = MetadataIO(PowerBiDataIO);

type PowerBiMetadata = io.TypeOf<typeof PowerBiMetadataIO>;

export const PowerBiComponent: FC<{ metadata: PowerBiMetadata }> = observer(
  ({ metadata }) => {
    const powerBiTokenState = useRef(appState.createPowerBiTokenState());
    const { data, fetch, isLoading } = powerBiTokenState.current;
    const { classes } = useStyles();
    const reportContainerRef = useRef(null);
    const [report, setReport] = useState(null);
    const { t } = useTranslation();
    const [tokenExpiration, setTokenExpiration] = useState(null);

    useEffect(() => {
      fetch({ params: { metadataId: metadata._id } });
    }, [metadata, fetch]);

    useEffect(() => {
      const interval = setInterval(() => {
        // Get the current time
        const currentTime = Date.now();
        const expiration = Date.parse(tokenExpiration);

        // Time until token expiration in milliseconds
        const timeUntilExpiration = expiration - currentTime;
        const timeToUpdate = MINUTES_BEFORE_EXPIRATION * 60 * 1000;

        // Update the token if it is about to expired
        if (timeUntilExpiration <= timeToUpdate) {
          fetch({ params: { metadataId: metadata._id } });
        }
      }, 30000);
      return () => clearInterval(interval);
    }, [metadata, fetch, tokenExpiration]);

    useEffect(() => {
      if (!data || !reportContainerRef.current) {
        return;
      }

      // Save token expiration
      setTokenExpiration(data.expiry);

      const config: IReportEmbedConfiguration = {
        accessToken: data.accessToken,
        embedUrl: data.embedUrl[0].embedUrl,
        id: data.embedUrl[0].reportId,
        tokenType: models.TokenType.Embed,
        pageName: metadata.data.page_id,
        type: "report",
        settings: {
          panes: {
            filters: {
              visible: false,
            },
            pageNavigation: {
              visible: false,
            },
          },
        },
      };

      if (!report) {
        // Create new report
        const report = window.powerbi.embed(reportContainerRef.current, config);
        setReport(report);
      } else {
        // Only update access token
        report.setAccessToken(data.accessToken);
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [reportContainerRef, data, metadata]);

    return (
      <AppLayout
        header={
          <DashboardHeader
            title={metadata.title}
            customTopBar={
              <IconButton
                title={t("showFullscreen")}
                color="primary"
                type="button"
                disabled={!report}
                onClick={() => report?.fullscreen()}
              >
                <Fullscreen />
              </IconButton>
            }
          />
        }
      >
        <ContainerVertical className={classes.relative}>
          {isLoading && <ContentLoader />}
          <div className={classes.reportContainer} ref={reportContainerRef} />
        </ContainerVertical>
      </AppLayout>
    );
  }
);

const useStyles = makeStyles({ name: { PowerBiComponent } })({
  relative: {
    position: "relative",
  },
  reportContainer: {
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    "& > iframe": {
      border: "none",
    },
  },
});
