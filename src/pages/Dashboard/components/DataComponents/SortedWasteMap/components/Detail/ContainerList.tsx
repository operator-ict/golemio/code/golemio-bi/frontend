import React, { FC } from "react";
import { SortedWasteContainer } from "codecs";
import { ChartGraphOption } from "../Chart";
import { ContainerDetail } from "./ContainerDetail";

export const ContainerList: FC<{
  containers: SortedWasteContainer[];
  measurementsRoute: string;
}> = ({ containers, measurementsRoute }) => {
  const [selectedTimeRange, setTimeRange] =
    React.useState<ChartGraphOption>(undefined);

  const [selectedPanel, selectPanel] = React.useState<number | null>(0);

  const handleChange =
    (panel: number) => (_event: React.ChangeEvent<{}>, isExpanded: boolean) => {
      selectPanel(isExpanded ? panel : null);
    };

  return (
    <>
      {containers.map((container, index) => (
        <ContainerDetail
          selectedTimeRange={selectedTimeRange}
          setTimeRange={setTimeRange}
          handleChange={handleChange(index)}
          measurementsRoute={measurementsRoute}
          container={container}
          isSelected={selectedPanel === index}
          key={index}
        />
      ))}
    </>
  );
};
