import React, { FC, ChangeEvent, useEffect, useState, useMemo } from "react";
import { SortedWasteContainer } from "codecs";
import moment from "moment";
import { useTranslation } from "react-i18next";
import { ContainerVertical } from "components";
import { ExpandMore, ErrorRounded } from "@mui/icons-material";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
} from "@mui/material";
import cls from "./detail.module.scss";
import { Chart, ChartGraphOption, chartGraphOptions } from "../Chart";
import { observer } from "mobx-react-lite";
import { createSortedWasteMeasurementsQueryStateObservable } from "state/modules";

export const ContainerDetail: FC<{
  container: SortedWasteContainer;
  isSelected: boolean;
  measurementsRoute: string;
  selectedTimeRange: ChartGraphOption;
  setTimeRange: (value: ChartGraphOption) => void;
  handleChange: (e: ChangeEvent, isExpanded: boolean) => void;
}> = observer(
  ({
    container,
    isSelected,
    measurementsRoute,
    selectedTimeRange,
    setTimeRange,
    handleChange,
  }) => {
    const { t } = useTranslation();

    const [sortedWasteMeasurements] = useState(
      createSortedWasteMeasurementsQueryStateObservable()
    );

    const trashType = container.trash_type
      ? container.trash_type.description
      : t("page::sortedWaste::trashType::unknown");

    useEffect(() => {
      const dateFrom = chartGraphOptions.reduce((min, curr) => {
        return min < curr.value ? min : curr.value;
      }, new Date());

      if (container && container.sensor_code) {
        sortedWasteMeasurements.fetch({
          params: {
            route: measurementsRoute,
          },
          queryParams: {
            containerId: "" + container.container_id,
            from: dateFrom.toISOString(),
          },
        });
      }
    }, [container, sortedWasteMeasurements, measurementsRoute]);

    const lastMeasurement = useMemo(() => {
      const measurement = sortedWasteMeasurements.data?.find(
        (measurement) => measurement.percent_calculated !== null
      );

      if (!measurement) {
        return null;
      }

      return {
        measuredAt: moment(measurement.measured_at_utc),
        value: measurement.percent_calculated,
      };
    }, [sortedWasteMeasurements.data]);

    if (!container.sensor_code) {
      return (
        <div className={cls.flexrow}>
          <Typography variant="h4" component="h3">
            {trashType}
          </Typography>
          <span className={cls.id}>KSNKO ID: {container.ksnko_id}</span>
        </div>
      );
    }

    return (
      <Accordion expanded={isSelected} onChange={handleChange}>
        <AccordionSummary
          classes={{
            root: `${cls.containerInfo} ${isSelected ? cls.selected : ""}`,
          }}
          expandIcon={<ExpandMore className={cls.expandIcon} />}
        >
          <div>
            <Typography variant="h4" component="h3">
              {trashType}
            </Typography>
            {container.sensor_code && (
              <span className={cls.id}>KSNKO ID: {container.ksnko_id}</span>
            )}
            {lastMeasurement && (
              <p className={cls.lastMeasurement}>
                {`${t(
                  "page::map::spotDetail::lastUpdate"
                )} ${lastMeasurement.measuredAt.fromNow()}`}
                {lastMeasurement.measuredAt < moment().subtract(1, "day") && (
                  <ErrorRounded color="error" fontSize="small" />
                )}
              </p>
            )}
          </div>
          {lastMeasurement && (
            <div className={cls.right}>
              <Typography variant="h4" component="span">
                {lastMeasurement.value}&nbsp;%
              </Typography>
            </div>
          )}
        </AccordionSummary>
        {container.sensor_code && (
          <AccordionDetails classes={{ root: cls.expansionDetails }}>
            <ContainerVertical className={cls.selected}>
              <Chart
                setTimeRange={setTimeRange}
                selectedTimeRange={selectedTimeRange}
                containerData={container}
                measurements={sortedWasteMeasurements}
              />
            </ContainerVertical>
          </AccordionDetails>
        )}
      </Accordion>
    );
  }
);
