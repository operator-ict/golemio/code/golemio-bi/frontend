import React, { useMemo } from "react";
import { MapWrapper } from "components/MapComponents";
import { MapGL } from "components/Map";
import { appState } from "state";
import { observer } from "mobx-react-lite";
import * as io from "io-ts";
import { MetadataIO } from "codecs";
import { DashboardHeader } from "pages/Dashboard/components/DashboardHeader";
import { AppLayout } from "components/AppLayout";

export const MapComponentMetadataIO = MetadataIO(io.unknown);

interface MapComponentProps {
  metadata: io.TypeOf<typeof MapComponentMetadataIO>;
}

export const MapComponent = observer((props: MapComponentProps) => {
  const { universalGeojsonQueryStateObservableFactory } = appState;
  const { metadata } = props;

  const universalGeojson = useMemo(
    () => universalGeojsonQueryStateObservableFactory(),
    [universalGeojsonQueryStateObservableFactory]
  );

  React.useEffect(() => {
    universalGeojson.fetch({ params: { route: metadata.route } });
  }, [universalGeojson, metadata]);

  return (
    <AppLayout header={<DashboardHeader title={metadata.title} />}>
      <MapWrapper
        isError={!!universalGeojson.error}
        isLoading={universalGeojson.isLoading}
      >
        {universalGeojson.data && <MapGL data={universalGeojson.data} />}
      </MapWrapper>
    </AppLayout>
  );
});
