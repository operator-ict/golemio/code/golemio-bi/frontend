import { useEffect, useMemo } from "react";
import { ParkingCollection, ParkingPropertiesIO, MetadataIO } from "codecs";
import { ParkingDetail } from "./components/ParkingDetail";
import {
    parkingIcon,
    parkingPaidIcon,
    parkingRideIcon,
} from "components/MapIcons";
import { MapWrapper } from "components/MapComponents";
import { MapGL } from "components/Map";
import { appState } from "state";
import { observer } from "mobx-react-lite";
import * as io from "io-ts";
import { AppLayout } from "components/AppLayout";
import { DashboardHeader } from "pages/Dashboard/components/DashboardHeader";

const getParkingIcon = (parkingTypeId: string | undefined) => {
    switch (parkingTypeId) {
        case "park_and_ride":
            return "parking-ride-marker";
        case "park_paid_private":
            return "parking-paid-marker";
        default:
            return "parking-marker";
    }
};

const transformData = (data: ParkingCollection) => {
    return {
        ...data,
        features: data.features.map((feature) => {
            return {
                geometry: feature.properties.centroid,
                properties: {
                    ...feature.properties,
                    icon: getParkingIcon(feature.properties.parking_type),
                },
                type: feature.type,
            };
        }),
    };
};

export const ParkingsMapMetadataIO = MetadataIO(io.unknown);

interface ParkingsMapProps {
    metadata: io.TypeOf<typeof ParkingsMapMetadataIO>;
}

export const ParkingsMap = observer((props: ParkingsMapProps) => {
    const { metadata } = props;
    const { universalGeojsonQueryStateObservableFactory } = appState;

    const universalGeojson = useMemo(
        () => universalGeojsonQueryStateObservableFactory(ParkingPropertiesIO),
        [universalGeojsonQueryStateObservableFactory]
    );

    useEffect(() => {
        const route = `${metadata.route}/?source=TSK&category=park_and_ride`;

        universalGeojson.fetch({ params: { route } });
    }, [universalGeojson, metadata]);

    const transformedData = useMemo(() => {
        return universalGeojson.data
            ? transformData(universalGeojson.data)
            : null;
    }, [universalGeojson.data]);

    return (
        <AppLayout header={<DashboardHeader title={metadata.title} />}>
            <MapWrapper
                isError={!!universalGeojson.error}
                isLoading={universalGeojson.isLoading}
            >
                {!!transformedData && (
                    <MapGL
                        data={transformedData}
                        RenderFeatureProp={({ properties }) => {
                            return <ParkingDetail properties={properties} />;
                        }}
                        customIcons={{
                            images: [
                                ["parking-marker", parkingIcon],
                                ["parking-ride-marker", parkingRideIcon],
                                ["parking-paid-marker", parkingPaidIcon],
                            ],
                            markerIcon: "{icon}",
                            clusterIcon: "parking-marker",
                        }}
                    />
                )}
            </MapWrapper>
        </AppLayout>
    );
});
