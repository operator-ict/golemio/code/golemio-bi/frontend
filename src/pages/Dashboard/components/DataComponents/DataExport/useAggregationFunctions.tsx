import { useTranslation } from "react-i18next";

export enum AggregationFunction {
  "AVG" = "avg",
  "SUM" = "sum",
  "COUNT" = "count",
  "MIN" = "min",
  "MAX" = "max",
}

export const useAggregationFunctions = () => {
  const { t } = useTranslation();

  return {
    aggregationFunctions: Object.values(AggregationFunction).map(
      (aggregation) => ({
        name: aggregation,
        label: t(`aggregation::${aggregation}`),
      })
    ),
  };
};
