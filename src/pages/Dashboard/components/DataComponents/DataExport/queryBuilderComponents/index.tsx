import { Controls } from "react-querybuilder";
import { QueryBuilderButton } from "./QueryBuilderButton";
import { QueryBuilderSelector } from "./QueryBuilderSelector";
import { QueryBuilderValueEditor } from "./QueryBuilderValueEditor";
import { QueryBuilderNotToggle } from "./QueryBuilderNotToggle";

export const queryBuilderControlElements: Partial<Controls> = {
  addGroupAction: QueryBuilderButton,
  addRuleAction: QueryBuilderButton,
  removeGroupAction: QueryBuilderButton,
  removeRuleAction: QueryBuilderButton,
  combinatorSelector: QueryBuilderSelector,
  fieldSelector: QueryBuilderSelector,
  operatorSelector: QueryBuilderSelector,
  valueEditor: QueryBuilderValueEditor,
  notToggle: QueryBuilderNotToggle,
};
