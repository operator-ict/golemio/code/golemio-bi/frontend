import { makeStyles } from "tss-react/mui";

export const useCommonQueryStyles = makeStyles()((theme) => {
  return {
    marginRight: {
      "&:not(last-child)": {
        marginRight: theme.spacing(1),
      },
    },
  };
});
