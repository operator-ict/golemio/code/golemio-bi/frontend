import React, {
    FC,
    useRef,
    useEffect,
    useState,
    useCallback,
    useMemo,
} from "react";
import { createDataExportStateObservable } from "state/modules/dataExport";
import { observer } from "mobx-react-lite";
import { AppLayout } from "components/AppLayout";
import { DashboardHeader } from "pages/Dashboard/components/DashboardHeader";
import { Footer } from "components/Footer";
import {
    CircularProgress,
    Typography,
    TextField,
    Box,
    Paper,
    Button,
    FormLabel,
    Select,
    MenuItem,
    FormControl,
    InputLabel,
    AccordionSummary,
    Accordion,
    AccordionDetails,
    FormHelperText,
} from "@mui/material";
import QueryBuilder, { RuleGroupTypeAny } from "react-querybuilder";
import { useTranslation, Trans } from "react-i18next";
import Autocomplete from "@mui/material/Autocomplete";
import { saveAs } from "file-saver";
import "./queryBuilder.scss";
import { queryBuilderControlElements } from "./queryBuilderComponents";
import { DataExportRequestBody, DataExportMetadata } from "codecs/DataExportIO";
import {
    useAggregationFunctions,
    AggregationFunction,
} from "./useAggregationFunctions";
import { DataExportPreview } from "./components/DataExportPreview";
import { ExpandMore, Edit, Add } from "@mui/icons-material";
import { appState } from "state";
import { MarkdownPreview } from "components/MarkdownPreview";
import { DataExportEditDialog } from "pages/Dashboard/components/DataComponents/DataExport/components/DataExportEditDialog";
import moment, { Moment } from "moment";
import { DateTime } from "components/DateTime";
import { makeStyles } from "tss-react/mui";

const DEBOUNCE_DELAY = 2000;
const DEFAULT_AGGREGATION = AggregationFunction.COUNT;

const useStyles = makeStyles()((theme) => {
    return {
        container: {
            flexWrap: "wrap",
            display: "flex",
            padding: theme.spacing(4),
        },
        options: {
            flex: "1 0 calc(40% - 16px)",
            padding: theme.spacing(4),
            marginBottom: theme.spacing(4),
            "& > *:not(last-child)": {
                marginBottom: theme.spacing(2),
            },
        },
        previewText: {
            width: "100%",
            overflowX: "auto",
        },
        aggregation: {
            border: "1px solid rgba(0, 0, 0, 0.23)",
            borderRadius: 4,
            padding: theme.spacing(1),
            listStyle: "none",
            display: "flex",
            flexWrap: "wrap",
        },
        aggregationSelect: {
            flex: "1 1 300px",
            marginBottom: theme.spacing(2),
        },
        timeSelect: {
            display: "inline-block",
            "&:not(:last-child)": {
                marginRight: theme.spacing(1),
            },
        },
    };
});

export const DataExport: FC<{ metadata: DataExportMetadata }> = observer(
    ({ metadata }) => {
        const { t } = useTranslation();
        const { classes } = useStyles();
        const state = useRef(createDataExportStateObservable(metadata.route));
        const { data, exportMetadata, preview } = state.current;
        const previewError = preview.error;
        const [query, setQuery] = useState<RuleGroupTypeAny | null>(null);
        const [columns, setColumns] = useState<string[]>([]);
        const [isPreviewDebouncing, setPreviewDebouncing] = useState(false);
        const [groupByColumns, setGroupByColumns] = useState<string[]>([]);
        const { aggregationFunctions } = useAggregationFunctions();
        const { admin } = appState.auth.user;
        const [isEditDialogOpen, setEditDialogOpen] = useState(false);
        const tagInputRef = useRef<HTMLElement | null>(null);
        const [selectedAggregations, setSelectAggregations] = useState<{
            [column: string]: AggregationFunction;
        }>({});

        const currentMoment = moment();
        const [limitRange, setLimitRange] = useState<{
            from: Moment;
            to: Moment;
        } | null>(
            metadata.data?.limit
                ? {
                      from: currentMoment
                          .clone()
                          .subtract(
                              metadata.data.limit.value,
                              metadata.data.limit.unit
                          ),
                      to: currentMoment,
                  }
                : null
        );

        useEffect(() => {
            exportMetadata.fetch({});
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, []);

        const handleLimitChange = useCallback(
            (value: string, name: "from" | "to") =>
                setLimitRange((oldValue) => ({
                    ...oldValue,
                    [name]: moment(value),
                })),
            []
        );

        const columnsToAggregate = useMemo(() => {
            if (!groupByColumns.length) {
                return [];
            }

            return columns.filter((column) => !groupByColumns.includes(column));
        }, [columns, groupByColumns]);

        const wrappedQuery = useMemo(() => {
            if (!metadata.data?.limit) {
                return query;
            }

            const { column } = metadata.data.limit;

            return {
                id: "wrapper",
                combinator: "and",
                rules: [
                    {
                        id: "time-range",
                        combinator: "and",
                        rules: [
                            {
                                field: column,
                                operator: ">=",
                                value: limitRange.from.toISOString(),
                            },
                            {
                                field: column,
                                operator: "<=",
                                value: limitRange.to.toISOString(),
                            },
                        ],
                    },
                    ...(query?.rules.length ? [query] : []),
                ],
            };
        }, [query, limitRange, metadata]);

        const maxRangeHelperText = useMemo(
            () =>
                metadata.data?.limit ? (
                    <Trans
                        components={[<strong />]}
                        values={{
                            range: t(metadata.data.limit.unit, {
                                count: metadata.data.limit.value,
                            }),
                        }}
                        i18nKey="maxRange"
                    />
                ) : null,
            [t, metadata]
        );

        const limitError = useMemo(() => {
            if (!metadata.data?.limit) {
                return null;
            }

            const { value, unit } = metadata.data.limit;
            const { from, to } = limitRange;

            if (to < from) {
                return t("wrongTimeRange");
            }

            if (to.clone().subtract(value, unit) > from) {
                return maxRangeHelperText;
            }

            return null;
        }, [limitRange, metadata, maxRangeHelperText, t]);

        const displayedPreviewError = useMemo(() => {
            if (!columns.length) {
                return t("noColumnChosen");
            }

            if (previewError) {
                return t("previewShowError", { error: previewError?.message });
            }

            if (limitError) {
                return limitError;
            }

            return null;
        }, [previewError, columns, limitError, t]);

        const body = useMemo((): DataExportRequestBody => {
            return {
                columns: columns.map((column) => {
                    if (!columnsToAggregate.includes(column)) {
                        return column;
                    }

                    const aggregation =
                        selectedAggregations[column] || DEFAULT_AGGREGATION;
                    return `${aggregation}(${column}) as ${column}`;
                }),
                builderQuery: wrappedQuery,
                groupBy: groupByColumns.length ? groupByColumns : undefined,
            };
        }, [
            wrappedQuery,
            columns,
            groupByColumns,
            selectedAggregations,
            columnsToAggregate,
        ]);

        const shouldNotFetch = (wrappedQuery): boolean => {
            const unaryOperator = ["null", "notNull"];
            return (
                !wrappedQuery ||
                (wrappedQuery.rules?.length &&
                    wrappedQuery.rules.some(
                        (el) =>
                            el.value === "" &&
                            !unaryOperator.includes(el.operator)
                    ))
            );
        };

        useEffect(() => {
            if (!columns.length || shouldNotFetch(wrappedQuery)) {
                return;
            }

            const timer = setTimeout(() => {
                setPreviewDebouncing(false);
                preview.fetch({ body });
            }, DEBOUNCE_DELAY);

            setPreviewDebouncing(true);

            return () => clearTimeout(timer);
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [wrappedQuery, columns, body]);

        useEffect(() => {
            setColumns(exportMetadata.data?.map((column) => column.name) || []);
        }, [exportMetadata.data]);

        const downloadData = useCallback(() => {
            if (data.isLoading) {
                return;
            }

            data.fetch({ body }).then((blob) =>
                saveAs(blob as Blob, `${metadata.title}.csv`, { autoBom: true })
            );
        }, [data, body, metadata]);

        const selectAggregation = useCallback(
            (column: string, aggregation: AggregationFunction) => {
                const newSelectedAggregations = {
                    ...selectedAggregations,
                    [column]: aggregation,
                };
                setSelectAggregations(newSelectedAggregations);
            },
            [selectedAggregations]
        );

        const columnDescription =
            metadata.data?.columnDescription?.replace(
                /\n/g,
                String.fromCharCode(10) // newline
            ) || "";

        return (
            <AppLayout
                header={<DashboardHeader title={metadata.title} />}
                footer={<Footer />}
            >
                <Box className={classes.container}>
                    {exportMetadata.isLoading && (
                        <Box display="flex" justifyContent="center">
                            <CircularProgress />
                        </Box>
                    )}
                    {exportMetadata.data && (
                        <>
                            <Paper className={classes.options}>
                                <Typography variant="h2">
                                    {t("exportOptions")}
                                </Typography>
                                <Typography>{metadata.description}</Typography>
                                {(!!columnDescription || admin) && (
                                    <Accordion>
                                        <AccordionSummary
                                            expandIcon={<ExpandMore />}
                                        >
                                            {t("columnDescription")}
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Box width="100%">
                                                <MarkdownPreview
                                                    source={columnDescription}
                                                />
                                                {admin && (
                                                    <Box textAlign="center">
                                                        <Button
                                                            type="button"
                                                            variant="contained"
                                                            startIcon={
                                                                columnDescription ? (
                                                                    <Edit />
                                                                ) : (
                                                                    <Add />
                                                                )
                                                            }
                                                            onClick={() =>
                                                                setEditDialogOpen(
                                                                    true
                                                                )
                                                            }
                                                        >
                                                            {columnDescription
                                                                ? t(
                                                                      "editColumnDescription"
                                                                  )
                                                                : t(
                                                                      "addColumnDescription"
                                                                  )}
                                                        </Button>
                                                        <DataExportEditDialog
                                                            initialMarkdown={
                                                                columnDescription
                                                            }
                                                            open={
                                                                isEditDialogOpen
                                                            }
                                                            metadata={metadata}
                                                            handleClose={() =>
                                                                setEditDialogOpen(
                                                                    false
                                                                )
                                                            }
                                                        />
                                                    </Box>
                                                )}
                                            </Box>
                                        </AccordionDetails>
                                    </Accordion>
                                )}
                                <div>
                                    <FormLabel>{t("columns")}</FormLabel>
                                    <Autocomplete
                                        multiple={true}
                                        options={exportMetadata.data.map(
                                            (column) => column.name
                                        )}
                                        value={columns}
                                        onChange={(_, v) => setColumns(v)}
                                        disableCloseOnSelect={true}
                                        ref={tagInputRef}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                fullWidth={true}
                                                ref={params.InputProps.ref}
                                                variant="outlined"
                                                placeholder={t("chooseColumns")}
                                                error={!columns.length}
                                            />
                                        )}
                                    />
                                </div>
                                <div>
                                    <FormLabel>{t("groupBy")}</FormLabel>
                                    <Autocomplete
                                        multiple={true}
                                        options={exportMetadata.data.map(
                                            (column) => column.name
                                        )}
                                        value={groupByColumns}
                                        onChange={(_, v) =>
                                            setGroupByColumns(v)
                                        }
                                        disableCloseOnSelect={false}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                fullWidth={true}
                                                variant="outlined"
                                                placeholder={t("chooseColumns")}
                                            />
                                        )}
                                    />
                                </div>
                                {columnsToAggregate.length > 0 && (
                                    <div>
                                        <FormLabel>
                                            {t("aggregationFunctions")}
                                        </FormLabel>
                                        <ul className={classes.aggregation}>
                                            {columnsToAggregate.map(
                                                (column) => (
                                                    <li
                                                        key={column}
                                                        className={
                                                            classes.aggregationSelect
                                                        }
                                                    >
                                                        <FormControl>
                                                            <InputLabel>
                                                                {column}
                                                            </InputLabel>
                                                            <Select
                                                                value={
                                                                    selectedAggregations[
                                                                        column
                                                                    ] ||
                                                                    DEFAULT_AGGREGATION
                                                                }
                                                                onChange={({
                                                                    target,
                                                                }) =>
                                                                    selectAggregation(
                                                                        column,
                                                                        target.value as AggregationFunction
                                                                    )
                                                                }
                                                            >
                                                                {aggregationFunctions.map(
                                                                    (agFn) => (
                                                                        <MenuItem
                                                                            key={
                                                                                agFn.name
                                                                            }
                                                                            value={
                                                                                agFn.name
                                                                            }
                                                                        >
                                                                            {
                                                                                agFn.label
                                                                            }
                                                                        </MenuItem>
                                                                    )
                                                                )}
                                                            </Select>
                                                        </FormControl>
                                                    </li>
                                                )
                                            )}
                                        </ul>
                                    </div>
                                )}
                                {!!metadata.data?.limit && (
                                    <div>
                                        <FormLabel>
                                            {t("exportLimit", {
                                                column: metadata.data.limit
                                                    .column,
                                            })}
                                        </FormLabel>
                                        <div>
                                            <DateTime
                                                error={!!limitError}
                                                className={classes.timeSelect}
                                                label={t("from")}
                                                defaultValue={limitRange.from}
                                                onChange={(value) =>
                                                    handleLimitChange(
                                                        value,
                                                        "from"
                                                    )
                                                }
                                            />
                                            <DateTime
                                                error={!!limitError}
                                                className={classes.timeSelect}
                                                label={t("to")}
                                                defaultValue={limitRange.to}
                                                onChange={(value) =>
                                                    handleLimitChange(
                                                        value,
                                                        "to"
                                                    )
                                                }
                                            />
                                        </div>
                                        <FormHelperText error={!!limitError}>
                                            {!!limitError
                                                ? limitError
                                                : maxRangeHelperText}
                                        </FormHelperText>
                                    </div>
                                )}
                                <div>
                                    <FormLabel>{t("rules")}</FormLabel>
                                    <QueryBuilder
                                        fields={exportMetadata.data}
                                        onQueryChange={(newQuery) =>
                                            setQuery(newQuery)
                                        }
                                        translations={t("queryBuilder", {
                                            returnObjects: true,
                                        })}
                                        controlElements={
                                            queryBuilderControlElements
                                        }
                                        resetOnFieldChange={false}
                                        resetOnOperatorChange={false}
                                    />
                                </div>
                                <Box display="flex" justifyContent="center">
                                    <Button
                                        disabled={
                                            !columns.length ||
                                            !!preview.error ||
                                            data.isLoading ||
                                            !!limitError
                                        }
                                        variant="contained"
                                        color="primary"
                                        onClick={downloadData}
                                        type="button"
                                    >
                                        <div>
                                            {data.isLoading ? (
                                                <CircularProgress size={20} />
                                            ) : (
                                                t("downloadExport")
                                            )}
                                        </div>
                                    </Button>
                                </Box>
                            </Paper>
                            <DataExportPreview
                                isLoading={preview.isLoading}
                                shouldReload={isPreviewDebouncing}
                                data={preview.data}
                                error={displayedPreviewError || undefined}
                            />
                        </>
                    )}
                </Box>
            </AppLayout>
        );
    }
);
