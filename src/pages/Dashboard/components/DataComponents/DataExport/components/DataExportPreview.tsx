import React, { FC, useMemo, ReactNode } from "react";
import {
  Paper,
  Typography,
  Box,
  CircularProgress,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TableContainer,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import { parse } from "papaparse";
import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => {
  return {
    preview: {
      flex: "1 0 100%",
      padding: theme.spacing(4),
      marginBottom: theme.spacing(4),
      maxWidth: "100%",
      [theme.breakpoints.up("xl")]: {
        maxWidth: "calc(60% - 16px)",
        marginLeft: theme.spacing(4),
      },
    },
    previewText: {
      width: "100%",
      overflowX: "auto",
    },
    absolutelyCentered: {
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
    },
    tableContainer: {
      maxHeight: "80vh",
    },
  };
});
export const DataExportPreview: FC<{
  isLoading: boolean;
  shouldReload: boolean;
  data?: string;
  error?: ReactNode;
}> = ({ data, isLoading, shouldReload, error }) => {
  const { classes } = useStyles();
  const { t } = useTranslation();
  const parsedData = useMemo(() => {
    if (!data) {
      return null;
    }

    const parseResult = parse<string[]>(data, {
      transform: (value) => (value === "" ? "," : `"${value}",`),
    });
    if (parseResult.errors.length === 0 && parseResult.data) {
      return parseResult.data;
    }
  }, [data]);

  return (
    <Paper className={classes.preview}>
      <Typography variant="h2">
        {t("preview")}
        {shouldReload && "*"}
      </Typography>
      {!!error && <Typography color="error">{error}</Typography>}
      <Box position="relative">
        {!error && parsedData ? (
          <>
            <TableContainer className={classes.tableContainer}>
              <Table size="small">
                {parsedData.length > 0 && (
                  <TableHead>
                    <TableRow>
                      {parsedData[0].map((value) => (
                        <TableCell key={value}>{value}</TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                )}
                {parsedData.length > 2 && (
                  <TableBody>
                    {parsedData.slice(1).map((row, rowIndex) => (
                      <TableRow key={rowIndex}>
                        {row.map((value, cellIndex) => (
                          <TableCell key={cellIndex}>{value}</TableCell>
                        ))}
                      </TableRow>
                    ))}
                  </TableBody>
                )}
              </Table>
            </TableContainer>
            {isLoading && (
              <CircularProgress className={classes.absolutelyCentered} />
            )}
          </>
        ) : (
          <>
            {isLoading && (
              <Box display="flex" justifyContent="center">
                <CircularProgress />
              </Box>
            )}
          </>
        )}
      </Box>
    </Paper>
  );
};
