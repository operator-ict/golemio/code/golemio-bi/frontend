import { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import * as io from "io-ts";
import { MetadataIO } from "codecs";
import { ContainerVertical, ContentLoader } from "components";
import { DashboardHeader } from "pages/Dashboard/components/DashboardHeader";
import { AppLayout } from "components/AppLayout";
import { useTranslation } from "react-i18next";

export const IframeComponentMetadataIO = MetadataIO(
  io.interface({
    url: io.string,
  })
);

interface IframeComponentProps {
  metadata: io.TypeOf<typeof IframeComponentMetadataIO>;
}

export const IframeComponent = observer((props: IframeComponentProps) => {
  const { metadata } = props;
  const { t } = useTranslation();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const iframe = document.getElementById("pbiFrame");
    const handleLoad = () => {
      setLoading(false);
    };
    iframe.addEventListener("load", handleLoad);
    return () => {
      iframe.removeEventListener("load", handleLoad);
    };
  }, []);
  return (
    <AppLayout header={<DashboardHeader title={metadata.title} />}>
      <ContainerVertical>
        {loading && <ContentLoader />}
        <iframe
          id="pbiFrame"
          title={t("content")}
          allowFullScreen={true}
          src={metadata.data.url}
          style={{ flex: "1 0 auto", display: loading ? "none" : "block" }}
        ></iframe>
      </ContainerVertical>
    </AppLayout>
  );
});
