import React, { useEffect, useState } from "react";
import { Routes, Route, Navigate, useLocation } from "react-router-dom";
import { Home } from "./Home";
import { observer } from "mobx-react-lite";
import { appState } from "state";
import { DataComponent } from "pages/Dashboard/components/DataComponents";
import { EditPage } from "pages/Dashboard/EditPage";
import TagManager from "react-gtm-consent-module";

enum Status {
  WAITING,
  LOADING,
  READY,
}

export const Dashboard = observer(() => {
  const { user, setUrlAfterLogin } = appState.auth;
  const { data, fetch: fetchMetadata, isLoading } = appState.metadata;
  const [state, setState] = useState<Status>(Status.WAITING);
  const { fetch: fetchTags } = appState.dashboardFilters;
  const location = useLocation();
  const { search } = appState.dashboardFilters;
  const [searchValue, setSearchValue] = useState(search);
  useEffect(() => setSearchValue(search), [search]);

  const tagManagerArgs = {
    dataLayer: {
      searchQuery: searchValue,
      event: 'SearchValue',
    },
    dataLayerName: 'user_id'
  }

  useEffect(() => {
    TagManager.dataLayer(tagManagerArgs)
  })

  useEffect(() => {
    if (!user) {
      setUrlAfterLogin(location.pathname + location.search + location.hash);
      return;
    }

    setUrlAfterLogin(null);

    fetchMetadata({ params: { userId: user.id } });
    fetchTags({ params: { userId: user.id } });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (isLoading && state !== Status.LOADING) {
      setState(Status.LOADING);
    }

    if (state === Status.LOADING && !isLoading) {
      setState(Status.READY);
    }
  }, [state, isLoading]);

  if (!user) {
    return <Navigate to="/auth" replace />;
  }

  return (
    <Routes>
      <Route element={<Home />} path="/" />
      <Route element={<EditPage />} path="/edit/:id" />
      <Route element={<EditPage />} path="/new" />
      {(data || []).map((metadata) => (
        <Route
          key={metadata.client_route}
          element={<DataComponent metadata={metadata} />}
          path={`/${metadata.client_route}`}
        />
      ))}
    </Routes>
  );
});
