import * as React from "react";
import { appState } from "state";
import cls from "./switch.module.scss";

export const LangSwitch = () => (
  <p style={{ display: "inline-block" }}>
    <span
      className={cls.clickable}
      onClick={() => appState.settings.setLng("cs")}
    >
      CS
    </span>
    {" / "}
    <span
      className={cls.clickable}
      onClick={() => appState.settings.setLng("en")}
    >
      EN
    </span>
  </p>
);
