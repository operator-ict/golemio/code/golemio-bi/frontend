import React from "react";
import TermsAndConditions from "../components/TermsAndConditions";

const PageTermsAndConditions = () => (
  <div style={{ margin: "100px auto", maxWidth: 800, padding: 30 }}>
    <TermsAndConditions />
  </div>
);

export default PageTermsAndConditions;
