import * as React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { PageSignUp } from "./SignUp";
import { SignIn } from "./SignIn";
import { PagePasswordReset } from "./PasswordReset";
import { PagePasswordResetRequest } from "./PasswordResetRequest";
import NotVerified from "./NotVerified";
import Verify from "./Verify";
import { PageAccountConfirmation } from "./AccountConfirmation";

import cls from "./auth.module.scss";

import { observer } from "mobx-react-lite";
import { appState } from "state";
import { AppLayout } from "components/AppLayout";
import { Footer } from "components/Footer";
import { AccessRequest } from "pages/Auth/AccessRequest";

interface AuthRoutesProps {
  enableSignUp: boolean;
}

export const AuthRoutes = observer((props: AuthRoutesProps) => {
  const { enableSignUp } = props;
  const { user, urlAfterLogin } = appState.auth;

  if (user && user.verified) {
    return <Navigate to={urlAfterLogin || "/dashboard"} />;
  }

  return (
    <AppLayout footer={<Footer />}>
      <div className={cls.pageWrapper}>
        <Routes>
          {enableSignUp && <Route element={<PageSignUp />} path="/sign-up" />}
          <Route element={<Verify />} path="/verify" />
          <Route element={<SignIn />} path="/sign-in" />
          <Route element={<AccessRequest />} path="/request-access" />
          <Route
            element={<PagePasswordReset />}
            path="/password-reset/:resetToken"
          />
          <Route
            element={<PagePasswordResetRequest />}
            path="/password-reset"
          />
          <Route
            element={<PageAccountConfirmation />}
            path="/account-confirmation/:confirmationToken"
          />
          <Route element={<NotVerified />} path="/not-verified" />
          <Route path="/" element={<Navigate to="/sign-in" replace />} />
        </Routes>
      </div>
    </AppLayout>
  );
});
