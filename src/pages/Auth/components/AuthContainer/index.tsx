import React, { FC, ReactNode } from "react";
import { AuthCarousel, CardData } from "components/AuthCarousel";
import { Paper, Box, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import ForwardIcon from "@mui/icons-material/Forward";
import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => {
  return {
    container: {
      display: "flex",
      justifyContent: "center",
      flexWrap: "wrap",
      maxWidth: "100%",
      width: 800,
      height: 550,
      overflow: "hidden",
      "&&& > *": {
        flex: "0 1 400px",
      },
    },
    innerContainer: {
      display: "flex",
      flexDirection: "column",
      minHeight: 578,
    },
    linkIcon: {
      transform: "rotate(180deg)",
      marginRight: 4,
    },
    backLink: {
      display: "flex",
      alignItems: "center",
      marginBottom: theme.spacing(2),
    },
  };
});
export const AuthContainer: FC<{
  cards?: CardData[];
  showBackLink?: boolean;
  children: ReactNode;
}> = ({ children, cards, showBackLink = false }) => {
  const { classes } = useStyles();
  const { t } = useTranslation();

  return (
    <Paper elevation={3} className={classes.container}>
      <Box
        className={classes.innerContainer}
        paddingX={5}
        paddingBottom={8}
        paddingTop={showBackLink ? 5 : 8}
      >
        {showBackLink && (
          <Typography
            color="textSecondary"
            className={classes.backLink}
            component={Link}
            to="/auth/sign-in"
          >
            <ForwardIcon className={classes.linkIcon} />
            {t("back")}
          </Typography>
        )}
        {children}
      </Box>
      <AuthCarousel cards={cards} />
    </Paper>
  );
};
