import { Button, TextField, Typography } from "@mui/material";
import { Formik } from "formik";
import { useFormStyles } from "hooks/formStyles";
import { observer } from "mobx-react-lite";
import { AuthContainer } from "pages/Auth/components/AuthContainer";
import React, { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { createAccessRequestState } from "state/modules";
import * as Yup from "yup";

interface IFormData {
    name: string;
    surname: string;
    email: string;
    organization: string;
    confirmation_password: string;
}

export const AccessRequest = observer(() => {
    const { t } = useTranslation();
    const { classes } = useFormStyles();
    const [state] = useState(createAccessRequestState());
    const { isLoading, fetch, wasSuccessful } = state;

    const sendRequest = useCallback(
        (formData: IFormData) => {
            if (isLoading) {
                return;
            }

            fetch({
                body: {
                    ...formData,
                },
            });
        },
        [isLoading, fetch]
    );

    return (
        <AuthContainer showBackLink={true}>
            <Formik
                initialValues={{
                    name: "",
                    surname: "",
                    email: "",
                    organization: "",
                    confirmation_password: "",
                }}
                onSubmit={sendRequest}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(),
                    surname: Yup.string().required(),
                    email: Yup.string().email().required(),
                    organization: Yup.string().required(),
                    confirmation_password: Yup.string().trim().matches(/^$/, {
                        message: "Validation error.",
                    }),
                })}
            >
                {({ handleSubmit, touched, handleBlur, handleChange, errors }) => (
                    <form onSubmit={handleSubmit} noValidate={true} className={classes.formWrapper}>
                        <Typography className={classes.bottomMargin} variant="h1">
                            {t("accessRequest")}
                        </Typography>
                        {!wasSuccessful && (
                            <>
                                <div>
                                    <TextField
                                        required={true}
                                        className={classes.bottomMargin}
                                        type="text"
                                        fullWidth={true}
                                        variant="outlined"
                                        name="name"
                                        label={t("name")}
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        error={touched.name && !!errors.name}
                                    />
                                    <TextField
                                        required={true}
                                        className={classes.bottomMargin}
                                        type="text"
                                        fullWidth={true}
                                        variant="outlined"
                                        name="surname"
                                        label={t("surname")}
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        error={touched.surname && !!errors.surname}
                                    />
                                    <TextField
                                        required={true}
                                        className={classes.bottomMargin}
                                        type="email"
                                        fullWidth={true}
                                        variant="outlined"
                                        name="email"
                                        label={t("email")}
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        placeholder={t("form::emailPlaceholder")}
                                        error={touched.email && !!errors.email}
                                    />
                                    <TextField
                                        required={true}
                                        className={classes.bottomMargin}
                                        type="text"
                                        fullWidth={true}
                                        variant="outlined"
                                        name="organization"
                                        label={t("organization")}
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        error={touched.organization && !!errors.organization}
                                    />
                                    <TextField
                                        tabIndex={-1}
                                        autoComplete="nope"
                                        className={classes.hfield}
                                        type="text"
                                        fullWidth={true}
                                        variant="outlined"
                                        name="confirmation_password"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                    />
                                </div>
                                <Button variant="contained" color="primary" disabled={isLoading} fullWidth={true} type="submit">
                                    {t("sendAccessRequest")}
                                </Button>
                            </>
                        )}
                        {wasSuccessful && (
                            <>
                                <Typography variant="h4" component="p" align="center">
                                    {t("accessRequestSentSuccessfully")}
                                </Typography>
                                <div />
                            </>
                        )}
                    </form>
                )}
            </Formik>
        </AuthContainer>
    );
});
