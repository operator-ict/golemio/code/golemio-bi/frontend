import { Button, Divider, Link, TextField, Typography } from "@mui/material";
import { Formik } from "formik";
import { useFormStyles } from "hooks/formStyles";
import { AuthContainer } from "pages/Auth/components/AuthContainer";
import React, { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { useAuthStateContext } from "state/modules/auth";
import * as validation from "validation";

export const SignIn = () => {
    const { t } = useTranslation();
    const { classes } = useFormStyles();
    const navigate = useNavigate();

    const appState = useAuthStateContext();
    const auth = appState.auth;
    const signingIn = auth.signInState.isLoading;
    const handleLogin = useCallback(
        (data: { email: string; password: string }) => {
            auth.signIn({ email: data.email, password: data.password })
                .then((data) => data && auth.decodeUserToken(data))
                .then((data) => data && auth.fetchUserState(data.id))
                .then((data: unknown) => {
                    if (data) {
                        navigate(auth.urlAfterLogin || "/dashboard");
                    }
                });
        },
        [auth, navigate]
    );

    return (
        <AuthContainer>
            <Formik
                initialValues={{
                    confirmation_password: "",
                    email: "",
                    password: "",
                }}
                onSubmit={handleLogin}
                validationSchema={validation.createSchema({
                    email: validation.email,
                    password: validation.password,
                    confirmation_password: validation.hfield,
                })}
            >
                {({ handleSubmit, handleBlur, handleChange, touched, errors }) => (
                    <form noValidate={true} onSubmit={handleSubmit} className={classes.formWrapper}>
                        <Typography variant="h1">{t("page::signin::title")}</Typography>
                        <div>
                            <TextField
                                className={classes.bottomMargin}
                                type="email"
                                fullWidth={true}
                                variant="outlined"
                                name="email"
                                label={t("email")}
                                placeholder={t("form::emailPlaceholder")}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                error={touched.email && !!errors.email}
                            />
                            <TextField
                                className={classes.bottomMargin}
                                type="password"
                                fullWidth={true}
                                variant="outlined"
                                name="password"
                                label={t("password")}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                error={touched.password && !!errors.password}
                            />
                            <TextField
                                tabIndex={-1}
                                autoComplete="nope"
                                className={classes.hfield}
                                type="text"
                                fullWidth={true}
                                variant="outlined"
                                name="confirmation_password"
                                onBlur={handleBlur}
                                onChange={handleChange}
                            />
                        </div>
                        <div>
                            <Button
                                className={classes.bottomMargin}
                                disabled={signingIn}
                                variant="contained"
                                fullWidth={true}
                                color="primary"
                                type="submit"
                            >
                                {t("signIn")}
                            </Button>
                            <Divider className={classes.bottomMargin} />
                            <Button
                                className={classes.bottomMargin}
                                variant="contained"
                                fullWidth={true}
                                component={RouterLink}
                                to="/auth/request-access"
                            >
                                {t("accessRequest")}
                            </Button>
                            <Link underline="always" color="textSecondary" component={RouterLink} to="/auth/password-reset">
                                {t("forgottenPassword")}
                            </Link>
                        </div>
                    </form>
                )}
            </Formik>
        </AuthContainer>
    );
};
