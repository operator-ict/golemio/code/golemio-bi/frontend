import { Button, CircularProgress, TextField, Typography } from "@mui/material";
import { Formik } from "formik";
import { useFormStyles } from "hooks/formStyles";
import { AuthContainer } from "pages/Auth/components/AuthContainer";
import React, { FC } from "react";
import { useTranslation } from "react-i18next";
import { useAuthStateContext } from "state/modules/auth";
import * as validation from "validation";

export const PagePasswordResetRequest: FC = () => {
    const { t } = useTranslation();
    const appState = useAuthStateContext();
    const [hasRequested, setHasRequested] = React.useState(false);
    const { classes } = useFormStyles();

    const { isLoading } = appState.auth.passwordResetRequestState;

    const handleClickReset = (data: { email: string }) => {
        appState.auth
            .passwordResetRequest({
                email: data.email,
            })
            .then((res: unknown) => {
                setHasRequested(!!res);
            });
    };

    return (
        <AuthContainer showBackLink={true}>
            {hasRequested ? (
                <>
                    <h2>{t("page::passwordRequest::titleSuccess")}</h2>
                    <p>{t("page::passwordRequest::asterixSuccess")}</p>
                </>
            ) : (
                <Formik
                    initialValues={{ email: "", confirmation_password: "" }}
                    onSubmit={handleClickReset}
                    validationSchema={validation.createSchema({
                        email: validation.email,
                        confirmation_password: validation.hfield,
                    })}
                >
                    {({ handleSubmit, handleChange, handleBlur, touched, errors }) => (
                        <form onSubmit={handleSubmit} noValidate={true} className={classes.formWrapper}>
                            <div>
                                <Typography className={classes.bottomMargin} variant="h1">
                                    {t("page::passwordRequest::title")}
                                </Typography>
                                <TextField
                                    className={classes.bottomMargin}
                                    type="email"
                                    fullWidth={true}
                                    variant="outlined"
                                    name="email"
                                    label={t("email")}
                                    placeholder={t("form::emailPlaceholder")}
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    error={touched.email && !!errors.email}
                                />
                                <TextField
                                    tabIndex={-1}
                                    autoComplete="nope"
                                    className={classes.hfield}
                                    type="text"
                                    fullWidth={true}
                                    variant="outlined"
                                    name="confirmation_password"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                />
                            </div>
                            <Button fullWidth={true} color="primary" type="submit" variant="contained">
                                {isLoading ? <CircularProgress size={20} /> : t("page::passwordRequest::resetButton")}
                            </Button>
                        </form>
                    )}
                </Formik>
            )}
        </AuthContainer>
    );
};
