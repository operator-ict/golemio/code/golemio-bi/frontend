import * as io from "io-ts";

export const FavouriteTilesIO = io.array(io.string);

export const userDataIO = io.partial({
  favouriteTiles: FavouriteTilesIO,
});
