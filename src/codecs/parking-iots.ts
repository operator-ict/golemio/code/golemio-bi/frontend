import * as io from "io-ts";
import { FeatureCollectionIO, PointIO } from "codecs/geojson-iots";
import { AddressIO } from "codecs/shared-iots";
import { maybe } from "./maybe";

/*interface HourBrand {
  readonly Hour: unique symbol;
}

const HourIO = io.brand(
  io.string,
  (s): s is io.Branded<string, HourBrand> => /([01][0-9]|2[0-3])/.test(s),
  'Hour',
);

interface WeekDayBrand {
  readonly WeekDay: unique symbol;
}

const WeekDayIO = io.brand(
  io.string,
  (s): s is io.Branded<string, WeekDayBrand> => /([0-6])/.test(s),
  'WeekDay',
);*/

// [00 - 23]: io.number
// const ParkingOccupancyDayIO = io.record(HourIO, maybe(io.number));
const ParkingOccupancyDayIO = io.record(io.string, maybe(io.number));

// [0 - 6]: ParkingOccupancyDayIO,
// const ParkingOccupancyIO = io.record(WeekDayIO, ParkingOccupancyDayIO);
const ParkingOccupancyIO = io.record(io.string, ParkingOccupancyDayIO);

const ParkingTypesIO = io.string || io.null;

export const ParkingCategoryIO = io.union([
    io.literal("park_and_ride"),
    io.literal("park_paid_private"),
    io.literal("zone_residential"),
    io.literal("zone_visitors"),
    io.literal("zone_mixed"),
    io.literal("zone_other"),
]);

export const ParkingPropertiesIO = io.intersection([
    io.interface({
        id: io.string,
        name: io.string,
        category: maybe(ParkingCategoryIO),
    }),
    io.partial({
        address: maybe(AddressIO), // added
        address_formatted: io.string, // added
        android_app_payment_url: maybe(io.string), // added
        area_served: maybe(io.string), // added
        available_spots_last_updated: maybe(io.string), // added
        available_spots_number: maybe(io.number), // added
        centroid: maybe(PointIO), // not solved for not needed
        // num_of_taken_places: maybe(io.number), // deprecated
        // num_of_taken_places: maybe(io.number), // deprecated
        // num_of_free_places: maybe(io.number), // deprecated
        // last_updated: maybe(io.string), // deprecated
        data_provider: io.string, // added
        date_modified: maybe(io.string), // added
        ios_app_payment_url: maybe(io.number), // added
        parking_type: maybe(ParkingTypesIO), // changed type from object {id: number, description: string} to string
        source: io.string, // added
        source_id: io.string, // added
        tariff_id: maybe(io.string), // added
        total_spot_number: io.number, // added
        valid_from: maybe(io.number), // added
        valid_to: maybe(io.number), // added
        web_app_payment_url: io.string, // added
        zone_type: maybe(io.string), // added
        average_occupancy: ParkingOccupancyIO,
    }),
]);

export const ParkingIO = FeatureCollectionIO(ParkingPropertiesIO);

export type ParkingCollection = io.TypeOf<typeof ParkingIO>;
export type ParkingProperties = io.TypeOf<typeof ParkingPropertiesIO>;
