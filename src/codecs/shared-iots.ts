import * as io from "io-ts";

export const AddressIO = io.partial({
  address_formatted: io.string,
  street_address: io.string,
  postal_code: io.string,
  address_locality: io.string,
  address_region: io.string,
  address_country: io.string,
});
