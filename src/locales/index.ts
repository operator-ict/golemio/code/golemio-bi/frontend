import translationCs from "./cs/translation.json";

export const resources = {
  cs: { translation: translationCs },
};
