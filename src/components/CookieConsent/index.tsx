import { FC, useEffect, useRef } from "react";
import "vanilla-cookieconsent";
import "../CookieConsent/cookieConsent.scss";

export const CookieConsent: FC = () => {
  let cc = useRef<any>();

  useEffect(() => {
    const win = window as Window &
      typeof globalThis & { initCookieConsent: () => any; CookieConsent: any };

    // ./assets/js/cookieconsent.js has to be load before
    if (!win.initCookieConsent) {
      return;
    }
    cc.current = win.initCookieConsent();
    win.CookieConsent = cc.current;

    cc.current.run({
      current_lang: "cs",
      autoclear_cookies: true, // default: false
      cookie_name: "golemiobi-cookieconsent", // default: 'cc_cookie'
      force_consent: false,
      // remove_cookie_tables: true,

      gui_options: {
        consent_modal: {
          layout: "bar", // box,cloud,bar
          position: "bottom center", // bottom,middle,top + left,right,center
          transition: "slide", // zoom,slide
        },
        settings_modal: {
          layout: "box", // box,bar
          // position: 'left',                // right,left (available only if bar layout selected)
          transition: "slide", // zoom,slide
        },
      },

      onAccept: (cookie: any) => {},

      onChange: (cookie: any, changed_preferences: any) => {},
      languages: {
        cs: {
          consent_modal: {
            title: "Používáme cookies",
            description:
              "Díky využití souborů cookies zjišťujeme, co je pro uživatele zajímavé. Analýza návštěvnosti nám pomáhá web neustále zlepšovat. Nepoužíváme cookies k marketingovým účelům, ani je nepředáváme nikomu dalšímu. <br><br> Dovolíte nám je takto používat?",
            primary_btn: {
              text: "Povolit vše",
              role: "accept_all", // 'accept_selected' or 'accept_all'
            },
            secondary_btn: {
              text: "Nastavení cookies",
              role: "c-settings", // 'settings' or 'accept_necessary'
            },
          },
          settings_modal: {
            title: "Přizpůsobit nastavení cookies",
            save_settings_btn: "Uložit nastavení",
            accept_all_btn: "Povolit vše",
            reject_all_btn: "Povolit nezbytné",
            close_btn_label: "Zavřít",
            blocks: [
              {
                title: "Nezbytně nutné cookies",
                description:
                  "Tyto cookies pomáhají, aby webová stránka byla použitelná a fungovala správně. Ve výchozím nastavení jsou povoleny a nelze je zakázat.",
                toggle: {
                  value: "functional_storage",
                  enabled: true,
                  readonly: true, // cookie categories with readonly=true are all treated as "necessary cookies"
                },
              },
              {
                title: "Statistika",
                description:
                  "Díky těmto cookies víme, kam u nás chodíte nejraději a máme statistiky o celkové návštěvnosti našich stránek.",
                toggle: {
                  value: "analytics_storage", // there are no default categories => you specify them
                  enabled: false,
                  readonly: false,
                },
              },
              // golemio.cz do not use marketing cookies
              // {
              //   title: "Marketing",
              //   description: "Pomocí těchto cookies dokážeme zjistit, jaké další webové stránky navštěvujete a informovat vás na těchto stránkách o našich novinkách nebo aktualizacích. Součástí jsou i cookies třetích stran, jako je Facebook, Google, Seznam.cz nebo jiní poskytovatelé.",
              //   toggle: {
              //     value: "marketing", // there are no default categories => you specify them
              //     enabled: false,
              //     readonly: false,
              //   },
              // },
            ],
          },
        },
      },
    });
  }, []);

  return (
    <div>
      <a
        href="/cookie"
        className="p-2"
        // onClick={() => cc.current && cc.current.showSettings()}
        data-cc="c-settings"
      >
        Cookies
      </a>
    </div>
  );
};
