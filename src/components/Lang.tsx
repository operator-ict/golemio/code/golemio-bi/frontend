/**
 * This Localization component initialises i18next
 * i18next: https://www.i18next.com/
 * react bindings for i18next: https://react.i18next.com/
 * i18next with plurals: http://docs.translatehouse.org/projects/localization-guide/en/latest/l10n/pluralforms.html?id=l10n/pluralforms
 *
 */

import * as React from "react";
import i18n from "i18next";
import detector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
import moment from "moment";
import "moment/locale/cs";
import { observer } from "mobx-react-lite";
import { appState } from "state";
import cs from "locales/cs/translation.json";
import { configuration } from "configuration";
import { FC, ReactNode } from "react";

const LANG_DEFAULT = configuration.DEFAULT_LANG || "cs";
const isDev = configuration.NODE_ENV !== "production";

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .use(detector)
  .init({
    /**
     * if there is no lng prop or no translation file, i18next will use a fallback language
     */
    fallbackLng: LANG_DEFAULT,
    resources: { cs: { translation: cs } },
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
    react: {
      bindI18n: "languageChanged",
    },
    keySeparator: "::",
    nsSeparator: "@@",
    debug: isDev,
  });

interface LangProps {
  children?: ReactNode;
}

export const Lang: FC<LangProps> = observer(({ children }) => {
  const lng = appState.settings.lng;

  React.useEffect(() => {
    i18n.changeLanguage(lng);
    moment.locale(lng);
  }, [lng]);

  return <>{children}</>;
});
