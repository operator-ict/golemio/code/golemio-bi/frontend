import React from "react";
import { Layer } from "react-map-gl";

interface ClusterProps {
    image: string;
    sourceId: string;
}

export const ClusterLayers = (props: ClusterProps) => {
    const { image, sourceId } = props;

    return (
        <>
            <Layer
                id={`${sourceId}-symbol`}
                type="symbol"
                source={sourceId}
                filter={["has", "point_count"]}
                layout={{
                    "icon-image": image,
                    "icon-allow-overlap": true,
                    "text-allow-overlap": true,
                }}
            />
            <Layer
                id={`${sourceId}-circle`}
                type="circle"
                source={sourceId}
                filter={["has", "point_count"]}
                paint={{
                    "circle-color": "#1b69d3",
                    "circle-radius": [
                        "step",
                        ["get", "point_count"],
                        10,
                        100,
                        13,
                    ],
                    // offset in px, has to be equal to (text-offset * text-size) to be centered
                    "circle-translate": [12, 12],
                }}
            />
            <Layer
                id={`${sourceId}-text`}
                type="symbol"
                source={sourceId}
                filter={["has", "point_count"]}
                layout={{
                    "text-field": "{point_count_abbreviated}",
                    // offset will be multiplied by text size
                    "text-offset": [1, 1],
                    "text-font": [
                        "DIN Offc Pro Medium",
                        "Arial Unicode MS Bold",
                    ],
                    "text-size": 12,
                    "icon-allow-overlap": true,
                    "text-allow-overlap": true,
                }}
                paint={{
                    "text-color": "#fff",
                }}
            />
        </>
    );
};
