import React from "react";
import cls from "./detail.module.scss";

interface SectionInfoProps {
  children: React.ReactNode;
}

export const SectionInfo = (props: SectionInfoProps) => {
  const { children } = props;
  return <div className={cls.sectionInfo}>{children}</div>;
};
