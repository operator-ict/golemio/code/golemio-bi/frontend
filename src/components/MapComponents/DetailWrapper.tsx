import React from "react";
import cls from "./detail.module.scss";
import { ClickAwayListener } from "@mui/material";

interface DetailProps {
    children: React.ReactNode;
    hideDetail: () => void;
}

export const DetailWrapper = (props: DetailProps) => {
    const { children, hideDetail } = props;

    return (
        <ClickAwayListener
            mouseEvent="onClick"
            onClickAway={() => {
                    hideDetail();
            }}
        >
            <div className={cls.wrapper}>
                {children}
                <span className={cls.close} onClick={hideDetail} role="button" tabIndex={0}>
                    &times;
                </span>
            </div>
        </ClickAwayListener>
    );
};
