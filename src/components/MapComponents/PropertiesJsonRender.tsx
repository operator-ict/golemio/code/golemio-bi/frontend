import React from "react";
import { JsonViewer } from "@textea/json-viewer";
import { ContentWrapper } from "./ContentWrapper";
import { useTranslation } from "react-i18next";

interface RenderProps {
  properties: object | null;
}

export const RenderJson = (props: RenderProps) => {
  const { properties } = props;
  const { t } = useTranslation();

  if (!properties) {
    return (
      <ContentWrapper>
        <h2>{t("page::sortedWaste::noData")}</h2>
      </ContentWrapper>
    );
  }

  return (
    <ContentWrapper>
      <JsonViewer
        value={properties}
        rootName="Properties"
        indentWidth={2}
        collapseStringsAfterLength={1}
        groupArraysAfterLength={100}
        displayDataTypes={false}
        objectSortKeys={true}
        enableClipboard={false}
        theme="dark"
      />
    </ContentWrapper>
  );
};
