import React, { FC, useState, useEffect } from "react";
import { Moment } from "moment";
import { TextField } from "@mui/material";

export const DateTime: FC<{
    error?: boolean;
    onChange: (value: string) => void;
    label: string;
    defaultValue: Moment;
    className?: string;
}> = ({ error, className, onChange, label, defaultValue }) => {
    const [date, setDate] = useState(defaultValue.format("YYYY-MM-DD"));
    const [time, setTime] = useState(defaultValue.format("HH:mm"));

    useEffect(() => {
        onChange(`${date}T${time}`);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [date, time]);

    return (
        <div className={className}>
            <TextField
                error={!!error}
                label={label}
                type="date"
                defaultValue={date}
                InputLabelProps={{ shrink: true }}
                onChange={(e) => setDate(e.currentTarget.value)}
            />
            <TextField
                error={!!error}
                type="time"
                label=" "
                defaultValue={time}
                InputLabelProps={{ shrink: true }}
                onChange={(e) => setTime(e.currentTarget.value)}
            />
        </div>
    );
};
