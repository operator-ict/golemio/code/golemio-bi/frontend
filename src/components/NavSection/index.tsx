import React, { ReactNode } from "react";
import classnames from "classnames";
import cls from "./nav-section.module.scss";

interface NavSectionProps {
  children?: ReactNode;
  className?: string;
  [prop: string]: unknown;
}

const NavSection = (props: NavSectionProps) => {
  const { className = null, children = null, ...rest } = props;

  return (
    <section className={classnames(cls.wrapper, className)} {...rest}>
      {children}
    </section>
  );
};

export default NavSection;
