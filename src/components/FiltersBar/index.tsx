import { FC } from "react";
import { Paper, Button } from "@mui/material";
import { TopicPicker } from "components/TopicPicker";
import { useMetadataTypeData } from "hooks/metadataTypeData";
import { observer } from "mobx-react-lite";
import { appState } from "state";
import classNames from "classnames";
import { SearchInput } from "components/SearchInput";
import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => {
  return {
    container: {
      padding: 20,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexWrap: "wrap",
    },
    categoryLink: {
      color: theme.palette.secondary.main,
      display: "flex",
      alignItems: "center",
      "&&& > *:first-of-type": {
        marginRight: 4,
      },
      "& svg": {
        fill: theme.palette.secondary.main,
      },
    },
    active: {
      backgroundColor: theme.palette.action.selected,
      color: theme.palette.primary.main,
      "& svg": {
        fill: theme.palette.primary.main,
      },
    },
    linkWrapper: {
      display: "flex",
      justifyContent: "space-between",
      flex: "1 1 auto",
      maxWidth: 800,
      flexWrap: "wrap",
      "& > *": {
        margin: 8,
      },
    },
    itemWrapper: {
      flex: "1 0 auto",
      "&:last-child": {
        textAlign: "right",
      },
    },
    icon: {
      height: 20,
    },
  };
});

export const FiltersBar: FC = observer(() => {
  const { classes } = useStyles();
  const { category, setCategory } = appState.dashboardFilters;
  const { metadataTypeData } = useMetadataTypeData();

  return (
    <Paper
      elevation={3}
      square={true}
      className={classes.container}
    >
      <div className={classes.itemWrapper}>
        <TopicPicker />
      </div>
      <div className={classes.linkWrapper}>
        {metadataTypeData.map((type) => (
          <Button
            key={type.type?.toString() || "all"}
            onClick={() => setCategory(type.type)}
            className={classNames(classes.categoryLink, {
              [classes.active]: type.type === category,
            })}
          >
            <type.Icon className={classes.icon} />
            {type.title}
          </Button>
        ))}
      </div>
      <div className={classes.itemWrapper}>
        <SearchInput />
      </div>
    </Paper>
  );
});
