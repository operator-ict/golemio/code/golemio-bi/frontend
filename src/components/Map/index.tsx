import React, {
    FC,
    ReactNode,
    useCallback,
    useMemo,
    useRef,
    useState,
} from "react";
import Map, { Source, Layer, NavigationControl, MapRef } from "react-map-gl";
import * as io from "io-ts";
import { FeatureCollectionIO, PropertiesIO } from "codecs";
import { defaultIcon, defaultClusterIcon } from "components/MapIcons";
import {
    DetailWrapper,
    RenderJson,
    MapImagesType,
    ClusterLayers,
} from "components/MapComponents";
import { MapLayerMouseEvent } from "mapbox-gl";
import { configuration } from "configuration";
import "mapbox-gl/dist/mapbox-gl.css";
import styles from "./Map.module.scss";

const DEFAULT_CENTER: [number, number] = [14.4378, 50.0755];
const DEFAULT_ZOOM: number = 10;
const MAX_BOUNDS: [number, number, number, number] = [
    13.38938, 49.46719, 15.662555, 50.57593,
];

interface RenderFeatureProps<FeatureProperties> {
    properties: FeatureProperties;
}

interface ICustomIcons {
    images: MapImagesType;
    markerIcon: string;
    clusterIcon?: string;
}

const DataIO = FeatureCollectionIO(PropertiesIO);
type FeatureCollection = io.TypeOf<typeof DataIO>;


interface customMapOptions<Data extends FeatureCollection> {
    data: Data;
    mapOptions?: any;
    children?: ReactNode;
    customIcons?: ICustomIcons;
    RenderFeatureProp?: FC<
        RenderFeatureProps<Data["features"][0]["properties"]>
    >;
}

const defaultImages: MapImagesType = [
    ["default-icon", defaultIcon],
    ["default-cluster-icon", defaultClusterIcon],
];

export const MapGL = (props: customMapOptions<any>) => {
    const { data, mapOptions, children, customIcons, RenderFeatureProp } =
        props;

    const { images, markerIcon, clusterIcon } = customIcons || {
        images: defaultImages,
        markerIcon: "default-icon",
        clusterIcon: "default-cluster-icon",
    };

    const [zoom, setZoom] = useState(DEFAULT_ZOOM);
    const mapRef = useRef<MapRef | null>(null);
    const mapRefCallback = useCallback(
        (ref: MapRef | null) => {
            if (ref !== null) {
                //Set the actual ref we use elsewhere
                mapRef.current = ref;
                const map = ref;

                const loadImage = () => {
                    images.forEach((image) => {
                        if (!map.hasImage(image[0])) {
                            map.loadImage(image[1].src, (error, result) => {
                                if (error) throw error;
                                map.addImage(image[0], result, {
                                    sdf: false,
                                });
                            });
                        }
                    });
                };

                loadImage();

                //TODO need this?
                map.on("styleimagemissing", (e) => {
                    loadImage();
                });
            }
        },
        [images]
    );
    const [viewState, setViewState] = useState({
        longitude: 14.4378,
        latitude: 50.0755,
        zoom: zoom,
    });

    const RenderFeature = useMemo(
        () => RenderFeatureProp || RenderJson,
        [RenderFeatureProp]
    );

    const [selectedFeature, selectFeature] = useState(null);

    const geoJsonSource = useMemo(
        () => ({
            type: data.type,
            features: data.features.map((item, featureIndex) => {
                // set feature index to recognize which object marker was clicked on
                return {
                    ...item,
                    properties: { ...item.properties, featureIndex },
                };
            }),
        }),
        [data]
    );

    const onFeatureClick = useCallback(
        (e: MapLayerMouseEvent) => {
            // stop clickAwayListener from closing detail instantly
            e.originalEvent.preventDefault();
            e.originalEvent.stopPropagation();

            const feature = e.features && e.features[0];
            const index = feature
                ? feature.properties && feature.properties.featureIndex
                : null;
            if (index !== null || index !== undefined) {
                selectFeature(data.features[index]);
            }
        },
        [data]
    );

    return (
        <div className={styles.map}>
            <Map
                {...viewState}
                onMove={(evt) => setViewState(evt.viewState)}
                style={{ width: "100%", height: "100%" }}
                mapStyle="mapbox://styles/mapbox/light-v8"
                mapboxAccessToken={configuration.MAPBOX_TOKEN}
                center={DEFAULT_CENTER}
                ref={mapRefCallback}
                maxBounds={MAX_BOUNDS}
                dragRotate={false}
                minZoom={9}
                interactiveLayerIds={["features-layer"]}
                onClick={onFeatureClick}
                getClusterExpansionZoom={true}
                onZoomEnd={(e) => setZoom(Math.round(e.viewState.zoom))}
                {...mapOptions}
            >
                <NavigationControl />
                <Source
                    id="data"
                    type="geojson"
                    data={geoJsonSource}
                    cluster={true}
                    clusterMaxZoom={18}
                    clusterRadius={50}
                />
                <Layer
                    source="data"
                    id="features-layer"
                    type="symbol"
                    filter={["!", ["has", "point_count"]]}
                    layout={{
                        "icon-image": markerIcon,
                        "text-offset": [0, 2],
                        "text-field": "{name}",
                        "text-letter-spacing": 0.05,
                        "text-font": [
                            "Roboto Regular",
                            "Arial Unicode MS Bold",
                        ],
                        "text-size": 12,
                        "text-anchor": "top",
                        "icon-allow-overlap": true,
                        "text-allow-overlap": true,
                    }}
                />
                <ClusterLayers
                    image={clusterIcon || markerIcon}
                    sourceId="data"
                />
                {children}
            </Map>
            {selectedFeature && RenderFeature && (
                <DetailWrapper hideDetail={() => selectFeature(null)}>
                    <RenderFeature properties={selectedFeature.properties} />
                </DetailWrapper>
            )}
        </div>
    );
};
