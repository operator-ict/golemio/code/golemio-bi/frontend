import React, { FC } from "react";
import ReactMarkdown from "react-markdown";
import gfm from "remark-gfm";
import {
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  TableContainer,
  Paper,
} from "@mui/material";

export const MarkdownPreview: FC<{ source: string }> = ({ source }) => {
  return (
    <ReactMarkdown
      remarkPlugins={[gfm]}
      components={{
        table: ({ children }) => (
          <TableContainer component={Paper}>
            <Table>{children}</Table>
          </TableContainer>
        ),
        tr: ({ children }) => <TableRow>{children}</TableRow>,
        thead: ({ children }) => <TableHead>{children}</TableHead>,
        tbody: ({ children }) => <TableBody>{children}</TableBody>,
        th: ({ children }) => (
          <TableCell align={"justify" || undefined}>{children}</TableCell>
        ),
      }}
      children={source}
    />
  );
};
