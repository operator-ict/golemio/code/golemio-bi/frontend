import React from "react";
import classnames from "classnames";

import cls from "./top-bar.module.scss";
import { observer } from "mobx-react-lite";

interface TopBarProps {
  center?: boolean;
  className?: string;
  children?: React.ReactNode;
  [prop: string]: unknown;
}

const TopBar = observer((props: TopBarProps) => {
  const { children = null, className = null, center = false, ...rest } = props;

  return (
    <div
      className={classnames(cls.wrapper, className, {
        [cls.center]: center,
      })}
      {...rest}
    >
      <div className={cls.container}>{children}</div>
    </div>
  );
});

export default TopBar;
