import { FC } from "react";
import CardMedia from "@mui/material/CardMedia";
import { MetadataType } from "codecs";
import { configuration } from "configuration";
import { makeStyles } from "tss-react/mui";
//
export const TileBanner: FC<{ type: MetadataType; thumbnailUrl?: string }> = ({
  type,
  thumbnailUrl,
}) => {
  const { classes } = useStyles();

  if (!thumbnailUrl) {
    return <div className={classes.banner} />;
  }

  return (
    <CardMedia
      image={`${configuration.API_URL}${thumbnailUrl}`}
      className={classes.banner}
    />
  );
};

const useStyles = makeStyles({ name: { TileBanner } })({
  banner: {
    width: "100%",
    height: 74,
  },
});
