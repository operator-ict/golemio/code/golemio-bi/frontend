import React, { FC, useMemo, useState, useCallback } from "react";
import {
  IconButton,
  Box,
  Card,
  CardHeader,
  CardContent,
  Typography,
  Chip,
  Menu,
  MenuItem,
} from "@mui/material";
import MoreHorizontalIcon from "@mui/icons-material/MoreHoriz";
import { Link } from "react-router-dom";
import { TileAvatar } from "./TileAvatar";
import { TileBanner } from "./TileBanner";
import { UnknownMetadata } from "codecs";
import { observer } from "mobx-react-lite";
import { appState } from "state";
import { useTranslation } from "react-i18next";
import { TileTooltip } from "components/Tiles/TileTooltip";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import ThumbDownIcon from "@mui/icons-material/ThumbDown";
import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => {
  return {
    title: {
      fontWeight: "bold",
      minHeight: 62,
    },
    tileWrapper: {
      flex: "0 1 333px",
      display: "flex",
      flexDirection: "column",
      position: "relative",
      margin: theme.spacing(2),
    },
    tile: {
      borderRadius: 10,
      display: "flex",
      flexDirection: "column",
      flex: "1 0 auto",
      width: "100%",
      textDecoration: "none",
      transition: "0.2s all ease-out",
    },
    clickableTile: {
      boxShadow: "0 0 10px #000a",
      "&:hover": {
        cursor: "pointer",
      },
    },
    disabledTile: {
      cursor: "not-allowed",
    },
    tileContent: {
      display: "flex",
      flex: "1 0 auto",
      justifyContent: "space-between",
      flexDirection: "column",
      alignItems: "flex-start",
    },
    tag: {
      borderRadius: 8,
      margin: theme.spacing(0.5),
      pointerEvents: "none",
    },
    tagHolder: {
      display: "flex",
      marginTop: "1.25rem",
    },
  };
});

export const Tile: FC<{
  tile: UnknownMetadata;
  showConfirmDeleteDialog: (tile: UnknownMetadata) => void;
}> = observer(({ tile, showConfirmDeleteDialog }) => {
  const { client_route, type, thumbnail, title, description, tags, _id } = tile;
  const { data } = appState.metadata;
  const { admin } = appState.auth.user;
  const { t } = useTranslation();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { favouriteTiles, addToFavourites, removeFromFavourites } =
    appState.userData;

  const clickable = useMemo(
    () =>
      !!data?.find(
        (userMetadata) => userMetadata.client_route === client_route
      ),
    [client_route, data]
  );

  const closeMenu = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const isFavourite = useMemo(
    () => favouriteTiles.includes(_id),
    [favouriteTiles, _id]
  );

  const { classes } = useStyles();

  return (
    <div className={classes.tileWrapper}>
      <TileAvatar type={type} />
      <Box textAlign="right" mr={2}>
        <>
          <IconButton
            onClick={({ currentTarget }) => setAnchorEl(currentTarget)}
            type="button"
            size="small"
          >
            <MoreHorizontalIcon />
          </IconButton>
          <Menu
            anchorOrigin={{ horizontal: "center", vertical: "top" }}
            // getContentAnchorEl={null}
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={closeMenu}
          >
            <MenuItem
              onClick={() => {
                isFavourite ? removeFromFavourites(_id) : addToFavourites(_id);
                closeMenu();
              }}
            >
              {isFavourite ? (
                <>
                  <ThumbDownIcon />
                  &nbsp;
                  {t("removeFromFavourites")}
                </>
              ) : (
                <>
                  <ThumbUpIcon color="primary" />
                  &nbsp;
                  {t("addToFavourites")}
                </>
              )}
            </MenuItem>
            {admin && (
              <MenuItem component={Link} to={`/dashboard/edit/${_id}`}>
                <EditIcon />
                &nbsp;
                {t("edit")}
              </MenuItem>
            )}
            {admin && (
              <MenuItem onClick={() => showConfirmDeleteDialog(tile)}>
                <DeleteIcon />
                &nbsp;
                {t("deleteTile")}
              </MenuItem>
            )}
          </Menu>
        </>
      </Box>
      <TileTooltip enabled={!clickable}>
        <Card
          className={`${classes.tile} ${
            clickable ? classes.clickableTile : classes.disabledTile
          }`}
          component={clickable ? Link : "div"}
          // @ts-ignore
          to={clickable ? `/dashboard${client_route}` : undefined}
        >
          <TileBanner type={type} thumbnailUrl={thumbnail?.url} />
          <CardHeader classes={{ title: classes.title }} title={title} />
          <CardContent className={classes.tileContent}>
            <Typography variant="body2">{description}</Typography>
            <div className={classes.tagHolder}>
              {tags.map((tag) => (
                <Chip
                  key={tag._id}
                  color="primary"
                  className={classes.tag}
                  label={tag.title.toLocaleUpperCase()}
                />
              ))}
            </div>
          </CardContent>
        </Card>
      </TileTooltip>
    </div>
  );
});
