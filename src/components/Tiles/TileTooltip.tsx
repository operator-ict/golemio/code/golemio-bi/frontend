import React, { FC, ReactElement } from "react";
import { useTranslation } from "react-i18next";
import { Tooltip } from "@mui/material";

export const TileTooltip: FC<{ enabled?: boolean; children: ReactElement }> = ({ enabled, children }) => {
    const { t } = useTranslation();

    if (!enabled) {
        return children;
    }

    return (
        <Tooltip placement="top" arrow={true} title={t("insufficientPermissions")}>
            {children}
        </Tooltip>
    );
};
