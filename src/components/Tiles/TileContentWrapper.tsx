import React, { FC, ReactNode } from "react";
import { makeStyles } from "tss-react/mui";
import ContainerVertical from "components/ContainerVertical";
import ScrollArea from "components/ScrollArea";
import { WidgetContainer } from "components";

type Props = {
  children: ReactNode;
};
export const TileContentWrapper: FC<Props> = ({ children }) => {
  const { classes } = useStyles();

  return (
    <ContainerVertical className={classes.centered}>
      <ScrollArea className={classes.fullWidth}>
        <WidgetContainer className={classes.stretched}>
          {children}
        </WidgetContainer>
      </ScrollArea>
    </ContainerVertical>
  );
};
const useStyles = makeStyles({ name: { TileContentWrapper } })({
  centered: {
    alignItems: "center",
  },
  stretched: {
    alignItems: "stretch",
  },
  fullWidth: {
    width: "100%",
  },
});
