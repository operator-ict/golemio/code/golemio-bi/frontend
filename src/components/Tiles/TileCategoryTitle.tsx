import React, { ReactNode } from "react";
import { Typography } from "@mui/material";
import { makeStyles } from "tss-react/mui";

interface Props {
  children?: ReactNode;
}
export const TileCategoryTitle: React.FC<Props> = ({ children }) => {
  const { classes } = useStyles();

  return (
    <div className={classes.categoryTitle}>
      <Typography variant="h3" component="h2">
        {children}
      </Typography>
    </div>
  );
};

const useStyles = makeStyles()((theme) => {
  return {
    categoryTitle: {
      width: theme.breakpoints.values.md,
      maxWidth: "100%",
      margin: "auto",
    },
  };
});
