import React, { FC, ReactNode } from "react";
import { makeStyles } from "tss-react/mui";
type Props = {
  children: ReactNode;
};

const useStyles = makeStyles()((theme) => {
  return {
    container: {
      maxWidth: theme.breakpoints.values.md,
      margin: "auto",
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center",
      width: "100%",
    },
  };
});

export const TileHolder: FC<Props> = ({ children }) => {
  const { classes } = useStyles();

  return <div className={classes.container}>{children}</div>;
};
