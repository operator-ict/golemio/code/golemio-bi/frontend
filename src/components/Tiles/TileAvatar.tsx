import React, { FC } from "react";
import { Avatar } from "@mui/material";
import { MetadataType } from "codecs";
import { getMetadataTypeColor } from "utils/metadataTypeColors";
import { useMetadataTypeData } from "hooks/metadataTypeData";
import { makeStyles } from "tss-react/mui";

export const TileAvatar: FC<{ type: MetadataType }> = ({ type }) => {
  const { metadataTypeData } = useMetadataTypeData();
  const { classes } = useStyles();

  const color = type ? getMetadataTypeColor(type) : "transparent";
  const Icon = metadataTypeData.find((item) => type === item.type)?.Icon;

  return (
    <Avatar style={{ backgroundColor: color }} className={classes.cardIcon}>
      {Icon ? <Icon className={classes.icon} /> : null}
    </Avatar>
  );
};
const useStyles = makeStyles({ name: { TileAvatar } })({
  cardIcon: {
    position: "absolute",
    left: 30,
    top: 0,
    width: 64,
    height: 64,
    pointerEvents: "none",
  },
  icon: {
    fill: "#fff",
  },
});
