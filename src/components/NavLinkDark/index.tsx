import React, { ReactNode } from "react";
import classnames from "classnames";
import cls from "./nav-link.module.scss";
import NavLink from "../NavLink";

interface NavLinkDarkProps {
  className?: string;
  children?: ReactNode;
  [prop: string]: unknown;
}

const NavLinkDark = (props: NavLinkDarkProps) => {
  const { className = null, children = null, ...rest } = props;

  return (
    <NavLink className={classnames(cls.wrapperDark, className)} {...rest}>
      {children}
    </NavLink>
  );
};

export default NavLinkDark;
