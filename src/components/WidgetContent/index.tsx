import React, { ReactNode, ElementType } from "react";
import classnames from "classnames";
import cls from "./widget-content.module.scss";

interface WidgetContentProps {
  children?: ReactNode;
  className?: string;
  Component?: ElementType;
  grey?: boolean;
  [prop: string]: unknown;
}

const WidgetContent = (props: WidgetContentProps) => {
  const {
    children = null,
    className = null,
    Component = "div",
    grey = false,
    ...rest
  } = props;

  return (
    <Component
      className={classnames(cls.wrapper, className, {
        [cls.grey]: grey,
      })}
      {...rest}
    >
      {children}
    </Component>
  );
};

export default WidgetContent;
