import React, { FC, useMemo } from "react";
import Typography from "@mui/material/Typography";
import { useTranslation } from "react-i18next";
import analysesImage from "./analyses.png";
import applicationsImage from "./applications.png";
import dashboardsImage from "./dashboards.png";
import dataExportImage from "./dataExport.png";
import Carousel from "react-material-ui-carousel";
import { makeStyles } from "tss-react/mui";
const DELAY = 8000;

export interface CardData {
  title?: string;
  description?: string;
  image: string;
}

const useDefaultCards = () => {
  const { t } = useTranslation();
  const defaultCards: CardData[] = [
    {
      title: t("dashboards"),
      description: t("dashboardsDescription"),
      image: dashboardsImage,
    },
    {
      title: t("dataExport"),
      description: t("dataExportDescription"),
      image: dataExportImage,
    },
    {
      title: t("applications"),
      description: t("applicationsDescription"),
      image: applicationsImage,
    },
    {
      title: t("analyses"),
      description: t("analysesDescription"),
      image: analysesImage,
    },
  ];

  return { defaultCards };
};

export const AuthCarousel: FC<{ cards?: CardData[] }> = ({ cards }) => {
  const { defaultCards } = useDefaultCards();
  const usedCards = useMemo(() => cards || defaultCards, [cards, defaultCards]);
  const { classes } = useStyles();

  return (
    <Carousel
      animation="slide"
      interval={DELAY}
      className={classes.container}
      indicatorContainerProps={{
        className: classes.indicatorContainer,
        style: {},
      }}
      duration={500}
    >
      {usedCards.map((usedCard) => (
        <div
          className={classes.container}
          style={{ backgroundImage: `url(${usedCard.image})` }}
          key={usedCard.image}
        >
          {usedCard.title && usedCard.description && (
            <div className={classes.card}>
              <Typography variant="h2" className={classes.cardTitle}>
                {usedCard.title}
              </Typography>
              <Typography variant="body2" color="textSecondary">
                {usedCard.description}
              </Typography>
            </div>
          )}
        </div>
      ))}
    </Carousel>
  );
};

const useStyles = makeStyles({ name: { AuthCarousel } })({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    zIndex: 0,
    height: 550,
    "&&& > .CarouselItem, &&& > .CarouselItem > div, &&& > .CarouselItem > div > div":
      {
        display: "flex",
        flex: "1 0 auto",
      },
  },
  image: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    backgroundPosition: "center",
    backgroundSize: "cover",
  },
  card: {
    padding: 20,
    backgroundColor: "#fffd",
    minHeight: 130,
  },
  cardTitle: {
    marginBottom: 10,
  },
  indicatorContainer: {
    position: "absolute",
    bottom: 8,
  },
});
