import React, { FC } from "react";
import { Typography, Paper } from "@mui/material";
import { makeStyles } from "tss-react/mui";
import { useTranslation } from "react-i18next";
import OICTLogo from "./logo_oict_new_black.svg";
import { CookieConsent } from "components/CookieConsent";

export const Footer: FC = () => {
  const { t } = useTranslation();
  const { classes } = useStyles();

  return (
    <Paper elevation={3} className={classes.wrapper}>
      <div className={classes.itemWrapper}>
        <Typography variant="body2">{t("golemioDescription")}</Typography>
      </div>
      <a
        href="https://operatorict.cz/"
        target="_blank"
        rel="noreferrer noopener"
      >
        <img alt="OICT logo" src={OICTLogo} height={32} />
      </a>
      <div className={classes.itemWrapper} />
      <CookieConsent />
    </Paper>
  );
};
const useStyles = makeStyles({ name: { Footer } })({
  wrapper: {
    padding: 16,
    display: "flex",
    alignItems: "center",
  },
  itemWrapper: {
    flex: "1 0 0",
    "&:last-child": {
      textAlign: "right",
    },
  },
});
