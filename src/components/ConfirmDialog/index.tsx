import React, { FC, ReactNode } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  CircularProgress,
} from "@mui/material";
import { useTranslation } from "react-i18next";

export const ConfirmDialog: FC<{
  title: string;
  isOpen: boolean;
  onClose: () => void;
  isLoading: boolean;
  onConfirm: () => void;
  confirmIcon: ReactNode;
  confirmText: string;
  children: React.ReactNode;
}> = ({
  title,
  isOpen,
  children,
  onClose,
  isLoading,
  onConfirm,
  confirmIcon,
  confirmText,
}) => {
  const { t } = useTranslation();

  return (
    <Dialog open={isOpen}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        <Button variant="contained" onClick={onClose} disabled={isLoading}>
          {t("cancel")}
        </Button>
        <Button
          disabled={isLoading}
          startIcon={
            isLoading ? (
              <CircularProgress color="inherit" size={20} />
            ) : (
              confirmIcon
            )
          }
          variant="contained"
          color="primary"
          onClick={onConfirm}
        >
          {confirmText}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
