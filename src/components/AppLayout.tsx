import React, { FC, ReactNode } from "react";
import { makeStyles } from "tss-react/mui";
import { Header } from "components/Header";

export const AppLayout: FC<{
  footer?: ReactNode;
  header?: ReactNode;
  children: React.ReactNode;
}> = ({ children, footer, header }) => {
  const { classes } = useStyles();

  return (
    <div className={classes.root}>
      {header || <Header />}
      <main className={classes.content}>{children}</main>
      {footer}
    </div>
  );
};
const useStyles = makeStyles({ name: { AppLayout } })({
  root: {
    display: "flex",
    minHeight: "100vh",
    flexDirection: "column",
  },
  content: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
  },
});
