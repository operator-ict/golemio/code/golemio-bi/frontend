import { AppPages as Pages } from "../pages";

import "./App.module.scss";
import { StyledEngineProvider, ThemeProvider } from "@mui/material/styles";
import { theme } from "../theme";
import { CssBaseline } from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import { configuration } from "configuration";
import { Lang } from "components/Lang";
import { SnackContainer } from "components/SnackContainer";
import TagManager, { TagManagerArgs } from "react-gtm-consent-module";
import { useEffect } from "react";

const tagManagerArgs: TagManagerArgs = {
  gtmId: window.ENV.GTM_ID,
  dataLayerName: 'user_id'
};


export const App = () => {

  useEffect(() => {
    TagManager.initialize(tagManagerArgs);

  }, [])

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Lang>
          <SnackContainer />
          <BrowserRouter basename={configuration.BASENAME}>
            <Pages />
          </BrowserRouter>
        </Lang>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};
