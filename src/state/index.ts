import { configure, observable } from "mobx";
import {
  districts,
  sortedWaste,
  vehiclePositions,
  metadata,
  universalGeojsonQueryStateObservableFactory,
  dashboardFilters,
  users,
  adminTags,
  createPowerBiTokenState,
  userData,
} from "state/modules";
import { auth } from "./modules/auth";
import { notifications } from "./modules/notifications";
import { settings } from "./modules/settings";

configure({ enforceActions: "always" });

export const appState = observable({
  sortedWaste,
  districts,
  settings,
  notifications,
  auth,
  vehiclePositions,
  metadata,
  dashboardFilters,
  universalGeojsonQueryStateObservableFactory,
  users,
  adminTags,
  createPowerBiTokenState,
  userData,
});
