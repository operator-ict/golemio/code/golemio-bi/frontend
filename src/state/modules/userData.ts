import { userDataIO } from "codecs/userData-iots";
import { configuration } from "configuration";
import {
  extendObservable,
  computed,
  autorun,
  action,
  AnnotationsMap,
} from "mobx";
import { createQueryStateObservable } from "state/createQueryStateObservable";
import { fetch } from "state/fetch";

export let userData;

const userDataQueryState = createQueryStateObservable({
  dataCodec: userDataIO,
  fetch: () => fetch(`${configuration.API_URL}/user`),
});

userData = extendObservable(
  userDataQueryState,
  {
    updateUserData: createQueryStateObservable({
      bodyCodec: userDataIO,
      fetch: ({ body }) =>
        fetch(`${configuration.API_URL}/user`, { body, method: "PUT" }),
    }),
    get favouriteTiles() {
      return userDataQueryState.data?.favouriteTiles || [];
    },
    addToFavourites(id: string) {
      if (
        this.error ||
        this.updateUserData.isLoading ||
        this.data?.favouriteTiles?.includes(id)
      ) {
        return;
      }

      const newFavouriteTiles = [...(this.data?.favouriteTiles || []), id];

      this.updateUserData.fetch({
        body: {
          ...this.data,
          favouriteTiles: newFavouriteTiles,
        },
      });
    },
    removeFromFavourites(id: string) {
      if (
        this.error ||
        this.updateUserData.isLoading ||
        !this.data?.favouriteTiles?.includes(id)
      ) {
        return;
      }

      this.updateUserData.fetch({
        body: {
          ...this.data,
          favouriteTiles: this.data.favouriteTiles.filter(
            (tileId) => tileId !== id
          ),
        },
      });
    },
  },
  {
    favouriteTiles: computed,
    addToFavourites: action.bound,
    removeFromFavourites: action.bound,
  } as AnnotationsMap<typeof userData, never>
);

let firstRun = true;
autorun(() => {
  const { isLoading, error } = userData.updateUserData;

  if (firstRun) {
    firstRun = false;
    return;
  }

  if (!isLoading && !error) {
    userData.fetch({});
  }
});
