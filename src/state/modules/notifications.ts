import { observable, action } from "mobx";
import { ReactNode } from "react";

export enum NotificationType {
  success = "success",
  error = "error",
}

export interface NotificationConfig {
  title: ReactNode;
  content?: ReactNode;
  type?: NotificationType;
  decayDuration?: number;
  replace?: string;
}

export interface Notification extends NotificationConfig {
  id: string;
}

export interface NotificationsState {
  notificationsMap: { [key: string]: Notification | undefined };
  notificationsDecayMap: { [key: string]: number | undefined };
  notificationsIdList: string[];
  notifications: Notification[];
  addNotification: (config: NotificationConfig) => string;
  updateNotification: (
    id: string,
    config: Pick<NotificationConfig, "title" | "content" | "type">
  ) => void;
  removeNotification: (id: string) => void;
  startNotificationDecay: (id: string) => void;
  stopNotificationDecay: (id: string) => void;
}

export const notifications = observable<NotificationsState>(
  {
    notificationsMap: {},
    notificationsDecayMap: {},
    notificationsIdList: [],
    get notifications() {
      return this.notificationsIdList.map(
        (id: string) => this.notificationsMap[id]
      );
    },
    addNotification({
      title,
      content,
      type = NotificationType.success,
      decayDuration = 4000,
      replace: replaceNotificationId,
    }: NotificationConfig) {
      const notificationId = `${Math.random() * 1000}`;
      this.notificationsMap[notificationId] = {
        id: notificationId,
        title,
        content,
        type,
        decayDuration,
      };
      this.notificationsIdList.push(notificationId);
      this.startNotificationDecay(notificationId);
      if (replaceNotificationId) {
        action("removeNotification2", () =>
          this.removeNotification(replaceNotificationId)
        )();
      }
      return notificationId;
    },
    updateNotification(id, { title, content, type }) {
      const n = notifications.notificationsMap[id];
      if (n) {
        this.notificationsMap[id] = { ...n, title, content, type };
      }
    },
    removeNotification(id) {
      this.notificationsMap[id] = undefined;
      const index = this.notificationsIdList.indexOf(id, 0);
      if (index > -1) {
        this.notificationsIdList.splice(index, 1);
      }
    },
    startNotificationDecay(id) {
      const { decayDuration } = this.notificationsMap[id] || {
        decayDuration: null,
      };
      if (decayDuration) {
        this.notificationsDecayMap[id] = window.setTimeout(
          action("removeNotification", () => this.removeNotification(id)),
          decayDuration
        );
      }
    },
    stopNotificationDecay(id) {
      window.clearTimeout(this.notificationsDecayMap[id]);
    },
  },
  {
    addNotification: action.bound,
    removeNotification: action.bound,
    startNotificationDecay: action.bound,
    stopNotificationDecay: action.bound,
    updateNotification: action.bound,
  }
);
