import * as io from "io-ts";
import { SortedWasteMeasurementIO } from "codecs/SortedWasteMeasurementIO";
import { createQueryStateObservable } from "state/createQueryStateObservable";
import { configuration } from "configuration";
import { fetch } from "state/fetch";

export const createSortedWasteMeasurementsQueryStateObservable = () =>
  createQueryStateObservable({
    paramsCodec: io.interface({ route: io.string }),
    dataCodec: io.array(SortedWasteMeasurementIO),
    queryParamsCodec: io.interface({
      containerId: io.string,
      from: io.string,
    }),
    fetch: ({ params: { route }, queryString }) =>
      fetch(`${configuration.PROXY_API_URL}${route}${queryString}`),
  });
