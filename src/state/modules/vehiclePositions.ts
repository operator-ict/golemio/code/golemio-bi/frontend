import { configuration } from "configuration";
import * as io from "io-ts";
import { autorun } from "mobx";
import { createQueryStateObservable } from "state/createQueryStateObservable";
import { fetch } from "state/fetch";
import { notifications, NotificationType } from "./notifications";

export const vehiclePositions = createQueryStateObservable({
  dataCodec: io.any,
  paramsCodec: io.interface({ route: io.string }),
  fetch: ({ params: { route }, queryString }) =>
    fetch(`${configuration.PROXY_API_URL}${route}${queryString}`),
});

autorun(() => {
  const error = vehiclePositions.error;
  if (error) {
    notifications.addNotification({
      title: error.message,
      type: NotificationType.error,
    });
  }
});
