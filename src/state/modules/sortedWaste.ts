// import { SortedWasteIO } from 'codecs';
import { extendObservable, autorun } from "mobx";
import * as io from "io-ts";
import { createQueryStateObservable } from "state/createQueryStateObservable";
import { notifications, NotificationType } from "./notifications";
import { configuration } from "configuration";
import { fetch } from "state/fetch";

const sortedWasteQueryStateObservable = createQueryStateObservable({
  dataCodec: io.any, // SortedWasteIO,
  paramsCodec: io.type({ route: io.string }),
  fetch: ({ params: { route }, queryString }) =>
    fetch(`${configuration.PROXY_API_URL}${route}${queryString}`),
});

export const sortedWaste = extendObservable(sortedWasteQueryStateObservable, {
  get districtList() {
    if (!sortedWasteQueryStateObservable.data) {
      return [];
    }
    return sortedWasteQueryStateObservable.data.features
      .reduce((districts, item) => {
        const { district } = item.properties;
        if (district && !districts.includes(district)) {
          districts.push(district);
        }
        return districts;
      }, [] as string[])
      .sort();
  },
});

autorun(() => {
  const error = sortedWaste.error;
  if (error) {
    notifications.addNotification({
      title: error.message,
      type: NotificationType.error,
    });
  }
});
