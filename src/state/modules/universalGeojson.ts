import * as io from "io-ts";
import { autorun } from "mobx";
import { FeatureCollectionIO, PropertiesIO } from "codecs";
import { createQueryStateObservable } from "state/createQueryStateObservable";
import { notifications, NotificationType } from "./notifications";
import { configuration } from "configuration";
import { fetch } from "state/fetch";

export const universalGeojsonQueryStateObservableFactory = (propertiesCodec?: io.Mixed) => {
  const state = createQueryStateObservable({
    dataCodec: FeatureCollectionIO(propertiesCodec ?? PropertiesIO),
    paramsCodec: io.interface({ route: io.string }),
    fetch: ({ params: { route }, queryString }) => fetch(`${configuration.PROXY_API_URL}${route}${queryString}`),
  });

  autorun(() => {
    const error = state.error;
    if (error) {
      notifications.addNotification({
        title: error.message,
        type: NotificationType.error,
      });
    }
  });

  return state;
};
