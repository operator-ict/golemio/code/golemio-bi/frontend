import { configuration } from "configuration";
import * as io from "io-ts";
import { extendObservable, runInAction } from "mobx";
import { createQueryStateObservable } from "state/createQueryStateObservable";
import { fetch } from "state/fetch";
import { notifications, NotificationType } from "./notifications";

export const createAccessRequestState = () => {
  const state = extendObservable(
    createQueryStateObservable({
      fetch: ({ body }) =>
        fetch(`${configuration.API_URL}/accessrequest`, {
          body,
          method: "POST",
        }).then((response) => {
          if (response.ok) {
            onSuccess();
          } else {
            notifications.addNotification({
              title: "accessRequestError",
              type: NotificationType.error,
            });
          }

          return response;
        }),
      bodyCodec: io.interface({
        name: io.string,
        surname: io.string,
        organization: io.string,
        email: io.string,
      }),
    }),
    {
      wasSuccessful: false,
    }
  );

  const onSuccess = () => {
    runInAction(() => {
      state.wasSuccessful = true;
    });
  };

  return state;
};
