import * as io from "io-ts";
import { PowerBITokenIO } from "codecs/powerBIToken-iots";
import { createQueryStateObservable } from "state/createQueryStateObservable";
import { configuration } from "configuration";
import { fetch } from "state/fetch";

export const createPowerBiTokenState = () =>
  createQueryStateObservable({
    paramsCodec: io.interface({ metadataId: io.string }),
    dataCodec: PowerBITokenIO,
    fetch: ({ params: { metadataId } }) =>
      fetch(
        `${configuration.API_URL}/metadata/${metadataId}/powerbi-embed-token`
      ),
  });
