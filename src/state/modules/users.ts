import { extendObservable, action, autorun } from "mobx";
import * as io from "io-ts";
import { UsersIO, UserIO } from "codecs/users-iots";
import { createQueryStateObservable } from "state/createQueryStateObservable";
import { auth } from "./auth";
import { configuration } from "configuration";
import { fetch } from "state/fetch";
type Annotations = {
  selectUser: (userId: number) => void;
};
type User = io.TypeOf<typeof UserIO>;

const usersQueryState = createQueryStateObservable({
  dataCodec: UsersIO,
  fetch: ({ queryString }) =>
    fetch(`${configuration.AUTH_API_URL}/users${queryString}`),
});

export const users = extendObservable(
  usersQueryState,
  {
    selectedUser: null as User | null,
    selectUser(userId: number): void {
      this.selectedUser =
        this.data?.find((user: User) => user.id === userId) || null;
    },
  } as Annotations & { selectedUser: User | null },
  { selectUser: action.bound }
);

autorun(() => {
  const { user } = auth;
  const { selectedUser, data } = users;

  if (!selectedUser && data && user) {
    users.selectUser(user.id);
  }
});
