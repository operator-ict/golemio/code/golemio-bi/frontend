import { action, extendObservable } from "mobx";
import * as io from "io-ts";
import { MetadataType } from "codecs";
import { TagIO } from "codecs/tag-iots";
import { createQueryStateObservable } from "state/createQueryStateObservable";
import { configuration } from "configuration";
import { fetch } from "state/fetch";

const tagQueryStateObservable = createQueryStateObservable({
  dataCodec: io.array(TagIO),
  paramsCodec: io.interface({ userId: io.number }),
  fetch: ({ params: { userId } }) =>
    fetch(`${configuration.API_URL}/user/${userId}/tag`),
});

export const dashboardFilters = extendObservable<
  {},
  Record<any, any> & {
    category: MetadataType;
    setCategory: (
      category: "dashboard" | "application" | "export" | "analysis"
    ) => void;
  }
>(
  tagQueryStateObservable,
  {
    selectedTags: [] as string[],
    toggleTagSelection(tagId: string) {
      this.search = "";
      const tagIndex = this.selectedTags.indexOf(tagId);
      if (tagIndex !== -1) {
        this.selectedTags = [
          ...this.selectedTags.slice(0, tagIndex),
          ...this.selectedTags.slice(tagIndex + 1),
        ];
      } else {
        this.selectedTags = [...this.selectedTags, tagId];
      }
    },
    category: null as MetadataType,
    setCategory(category: MetadataType) {
      this.category = category;
    },
    search: "",
    setSearch(search: string) {
      this.selectedTags = [];
      this.category = null;
      this.search = search;
    },
  },
  {
    toggleTagSelection: action.bound,
    setCategory: action.bound,
    setSearch: action.bound,
  }
);
