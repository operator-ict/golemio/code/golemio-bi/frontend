import * as Yup from "yup";

export const email = Yup.string().email("form::notAnEmail").required("form::required");

export const password = Yup.string().required("form::required");

export const hfield = Yup.string().trim().matches(/^$/, {
    message: "Validation error.",
});

const validations: { [key: string]: Yup.StringSchema<string> } = {
    email,
    password,
};

export const validate = (key: string) => (value: unknown) =>
    validations[key]
        .validate(value)
        .then(() => null)
        .catch((e: Error) => e.message);

export const createSchema = (objectSchema: {}) => Yup.object().shape(objectSchema);
