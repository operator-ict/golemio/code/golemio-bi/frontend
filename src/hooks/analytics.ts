import { configuration } from "configuration";
import ReactGA from "react-ga";

const key = configuration.GA_KEY;

if (key) {
  ReactGA.initialize(key);
}

export const useAnalytics = () => {
  return {
    pageview: key ? (page: string) => ReactGA.pageview(page) : () => {},
  };
};
