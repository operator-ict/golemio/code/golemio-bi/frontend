import { makeStyles } from "tss-react/mui";

export const useFormStyles = makeStyles()((theme) => {
  return {
    bottomMargin: {
      marginBottom: theme.spacing(2),
    },
    hfield: {
      opacity: 0,
      position: "absolute",
      top: 0,
      left: 0,
      height: 0,
      width: 0,
      zIndex: -1,
      marginBottom: theme.spacing(2),
    },
    formWrapper: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-between",
      flex: "1",
      zIndex: "0",
    },
  };
});
