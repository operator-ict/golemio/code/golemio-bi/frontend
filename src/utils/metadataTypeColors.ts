import { MetadataType } from "codecs";

const getColor = (type: MetadataType) => {
  switch (type) {
    case "analysis":
      return "#D41A1A";
    case "application":
      return "#36D9BF";
    case "dashboard":
      return "#1CB5C4";
    case "export":
      return "#090942";
  }
};

export const getMetadataTypeColor = (
  type: MetadataType,
  light: boolean = false
) => {
  const color = getColor(type);
  return light ? `${color}1A` : color;
};
